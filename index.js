import React from 'react';
import { AppRegistry } from 'react-native';
import App from './src/app';
import {Provider} from 'react-redux';
import {store} from './src/config/store';
import { YellowBox } from 'react-native';
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);
import EStyleSheet from 'react-native-extended-stylesheet'
EStyleSheet.build({
    $font :'IRANSansMobile(FaNum)',
    $mainColor : 'rgb(163, 43, 250)'
})
const Application = () => {
    return (
        <Provider store={store}>
            <App/>
        </Provider>
    );
};
AppRegistry.registerComponent('behya', () => App);