import React, {Component} from 'react';
import { Container , Header , View , Text , Left , Button , Right , Content , Form , Item , Input } from 'native-base';
import {chat} from "../assets/styles";
import {AsyncStorage, BackHandler, Image, StatusBar, TouchableOpacity} from "react-native";
import { Actions } from 'react-native-router-flux';
import {pexios, url} from "../components/helper";
import Icon from 'react-native-vector-icons/dist/Feather'

export default class chatUser extends Component {
    constructor(props) {
        super(props)
        this.state = {
            users:[],
            yourId:null,
        }
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        // pexios('/message/list','post').then(res=>{
        //     this.setState({
        //         users:res.data.data.message,
        //         yourId:res.data.data.userId,
        //     })
        // })
    }
    handleBackButton(){
        Actions.pop()
        return true;
    }
    render() {
        return (
            <Container style={chat.container}>
                {/*<StatusBar backgroundColor="white" barStyle="dark-content"/>*/}

                <View style={chat.header}>
                    {
                        <View style={{flexDirection:'row-reverse',alignItems:'center'}}>
                            <Text style={[chat.userName, {paddingRight: '37%'}]}>پیام خصوصی</Text>
                        </View>

                    }
                    <TouchableOpacity onPress={()=>Actions.pop()}><Icon name='chevron-left' color="white" size={30} /></TouchableOpacity>
                </View>
                <View style={chat.userChatParent}>
                    {
                        this.state.users.length?
                            this.state.users.map((itm)=>{
                                return(
                                    <Item key={itm.id}>
                                        <TouchableOpacity style={chat.userButton} onPress={()=>Actions.chat({id:itm.from_user.id!=this.state.yourId?itm.from_user.id:itm.to_user.id})} >
                                            <Image source={{ uri : itm.from_user.id!=this.state.yourId?itm.from_user.attachments?url+'/files?uid='+itm.from_user.attachments.uid+'&width=64&height=64': url+'/images/profile.png':itm.to_user.attachments?url+'/files?uid='+itm.to_user.attachments.uid+'&width=64&height=64': url+'/images/profile.png'}} style={chat.userImg} />
                                            <View>
                                                <Text style={chat.userName}>{itm.from_user.id!=this.state.yourId?itm.from_user.fname:itm.to_user.fname} {itm.from_user.id!=this.state.yourId?itm.from_user.lname:itm.to_user.lname}</Text>
                                                <Text style={chat.userChat}>{itm.text}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </Item>
                                )
                            })
                            :<Text style={[chat.empty, {textAlign: 'center', paddingTop: 50}]}>پیامی در صندوق پیام شما وجود ندارد</Text>
                    }
                </View>
            </Container>
        )
    }
}