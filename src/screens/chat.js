import React, {Component} from 'react';
import {
    Container,
    Header,
    View,
    Text,
    Left,
    Button,
    Right,
    Content,
    Form,
    Item,
    Input,
    Textarea,
    Footer,
    Spinner
} from 'native-base';
import {chat, list} from "../assets/styles";
import Icon from 'react-native-vector-icons/dist/Feather'
import {AsyncStorage, BackHandler, FlatList, Image, ScrollView, StatusBar, TouchableOpacity} from "react-native";
import { Actions } from 'react-native-router-flux';
import {pexios, url} from "../components/helper";
import Chat from "../components/chat";
import profile from "../assets/profile.png";

export default class chatUser extends Component {
    constructor(props) {
        super(props)
        this.state = {
            user:null,
            refreshing:false,
            chats:[],
            chatMessage:'',
            myUser:null,
            disabled:false,
        }
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);

        // pexios('/core/get-user?id='+this.props.id,'get').then(res=>{
        //     this.setState({
        //         user:res.data.data.user,
        //     },()=>{
        //         this.getChatRequest()
        //     })
        // })
    }
    handleBackButton(){
        Actions.pop()
        return true;
    }
    render() {
        console.log('user indnnnnnnn chat', this.props.user)
        console.log('user indnnnnnnn chat fname', this.props.user.fname)
        return (
                <Container style={chat.container}>
                    {/*<StatusBar backgroundColor="#f2c200" barStyle="dark-content"/>*/}
                    <View style={chat.header}>
                        {
                                <View style={{flexDirection:'row-reverse',alignItems:'center'}}>
                                    <Image source={profile} style={chat.userImg} />
                                    <Text style={chat.userName}>{this.props.user.fname} {this.props.user.lname}</Text>
                                </View>

                        }
                        <TouchableOpacity onPress={()=>Actions.pop()}><Icon name='chevron-left' color="white" size={30} /></TouchableOpacity>
                    </View>
                    <Content keyboardShouldPersistTaps='handled' contentContainerStyle={{ flexGrow: 1 }} style={{padding:5}}>
                        <FlatList
                            data={this.state.chats}
                            renderItem={this.renderItem.bind(this)}
                            keyExtractor={(item) => item.id.toString()}
                            refreshing={this.state.refreshing}
                            onRefresh={this.handleRefresh.bind(this)}
                            style={{  transform: [{ scaleY: -1 }],flex:1 }}
                        />
                    </Content>
                    <View style={chat.bottom}>
                        <View style={{flexDirection:'row',flex:1,marginLeft:15}}>
                            <Textarea placeholder='متن' style={chat.textArea} onChangeText={(value)=>this.setState({chatMessage:value})} value={this.state.chatMessage} />
                        </View>
                        <Button style={chat.sendBtn} onPress={this.storeMessage.bind(this)} disabled={this.state.disabled}>
                            {
                                this.state.disabled?
                                    <Spinner color='#fff' />
                                    :
                                    <Text style={chat.sendBtnText}>ارسال</Text>
                            }
                        </Button>
                    </View>
                </Container>
        )
    }
    getChatRequest() {
        pexios('/message/show?user_id='+this.props.id,'get').then(res=>{
                this.setState({
                    chats: res.data.data.message,
                    refreshing : false,
                    myUser:res.data.data.userId
                })
            })
    }
    renderItem({ item }) {
        return <Chat ch={item} user={this.state.myUser}/>
    }

    handleRefresh(){
        this.setState({ refreshing : true } , () => {
            this.getChatRequest();
        })
    }
    storeMessage(){
        this.setState({
            disabled:true
        })
            if(this.state.chatMessage!=''){
            // pexios('/message/store','post',{to:this.props.id,text:this.state.chatMessage}).then(res=>{
            //     let ch = res.data.data;
            //     this.state.chats.reverse()
            //     this.state.chats.push(ch)
            //     this.state.chats.reverse()
            //     this.forceUpdate()
            //     this.setState({
            //         chatMessage:'',
            //         disabled:false
            //     })
            // })
        }

    }
}