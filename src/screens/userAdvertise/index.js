import React, {Component} from 'react';
import {ScrollView, View, ImageBackground, Alert, Text, TouchableOpacity, TextInput, AsyncStorage} from 'react-native';
import {connect} from 'react-redux';
import Loader from '../../components/loader/index'
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import Advertise from '../../components/advertise'
import Axios from 'axios'
import url from '../../config/address'
Axios.defaults.baseURL = url;
import FIcon from 'react-native-vector-icons/dist/Feather';


class UserAdvertise extends Component {
    constructor(props){
        super(props);
        this.state = {
            list: [],
            loading: false,
            result: [],
            search: false,
            text: ''
        };
    }
    componentWillMount() {
        this.setState({loading: true });

        AsyncStorage.getItem('token').then((info) => {
            if(info !== null) {
                const newInfo = JSON.parse(info);
                Axios.post('/myadds', {
                    user: newInfo.token
                }).then(response=> {
                    console.log('user advertise', response.data)
                    this.setState({list: response.data, loading: false});
                })
                .catch((error) => {
                    Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                    this.setState({loading: false});
                });
            }
            else {

            }
        });
    }
    startSearch(text){
        if(this.state.text === '') {
            Alert.alert('', 'عبارت مورد جستجو نمی تواند خالی باشد')
        }
        else {
            this.setState({ loading: true});
            const result = this.state.list.filter(item => item.title.includes(this.state.text));
            console.log('result', result)
            this.setState({loading: false, search: true, result: result});
        }
    }
    render() {
        if(this.state.loading)
            return (<Loader/>)
        else return(
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={styles.top}>
                        <Icon name="bell" size={20} color="black" />
                        <View style={{width: '80%',  position: 'relative', zIndex: 1}}>
                            <TextInput
                                maxLength={15}
                                placeholder={'جستجو در ویترین ها'}
                                placeholderTextColor={'gray'}
                                underlineColorAndroid='transparent'
                                value={this.state.text}
                                style={{
                                    height: 35,
                                    width: '90%',
                                    color: 'gray',
                                    fontSize: 14,
                                    textAlign: 'right',
                                    borderRadius: 20,
                                    backgroundColor: 'rgb(239, 239, 239)',
                                    paddingRight: 10
                                }}
                                onChangeText={(text) => this.setState({text: text})}
                            />
                            <TouchableOpacity onPress={() => this.startSearch(this.state.text)} style={{position: 'absolute', zIndex: 9992, top: '13%', left: '3%'}}>
                                <FIcon name="search" size={20} color="gray"  />
                            </TouchableOpacity>
                        </View>
                        <TouchableOpacity onPress={() => Actions.drawerOpen()}>
                            <Icon name="align-right" size={20} color="black" />
                        </TouchableOpacity>
                    </View>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.bodyContainer}>
                        <View style={{flexDirection: 'row', flexWrap: 'wrap', padding: 10, paddingBottom: 200}}>
                            {
                                this.state.search ? (
                                    this.state.result.length !==0 ?  this.state.result.map((item, index) => <Advertise key={index} title={item.title} item={item} />)
                                        : <Text style={[styles.advLabel, {paddingLeft: '35%'}]}>موردی یافت نشد</Text>
                                ) :
                                    this.state.list.length !== 0 ?

                                        this.state.list.map((item, index) => <Advertise key={index} title={item.title} item={item} />)
                                            : <Text style={styles.advLabel}>آگهی برای نمایش وجود ندارد</Text>
                            }
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}
export default UserAdvertise;
