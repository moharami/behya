import React, {Component} from 'react';
import {ScrollView, View, ImageBackground, Alert, Text, TouchableOpacity, TextInput, AsyncStorage} from 'react-native';
import {connect} from 'react-redux';
import Loader from '../../components/loader/index'
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import AppSearchBar from '../../components/searchBar/index'
import Axios from 'axios'
import url from '../../config/address'
Axios.defaults.baseURL = url;
import FIcon from 'react-native-vector-icons/dist/Feather';

class Vitrin extends Component {
    constructor(props){
        super(props);
        this.state = {
            list: [],
            loading: false,
            active: '',
            result: [],
            search: false,
            text: ''
        };
    }
    componentWillMount() {
        this.setState({loading: true });
        if(this.props.loged) {
            AsyncStorage.getItem('token').then((info) => {
                if(info !== null) {
                    const newInfo = JSON.parse(info);
                    Axios.post('/my_vitrins', {
                        user: newInfo.token
                    }).then(response=> {
                        console.log('list', response.data)
                        this.setState({loading: false, list: response.data});
                    })
                    .catch((error) => {
                        this.setState({ loading: false});
                    });
                }
                else {
                    this.setState({loading: false});
                }
            });
        }
        else {
            Axios.get('/vitrin_list'
            ).then(response=> {
                this.setState({loading: false, list: response.data});
            })
                .catch((error) => {
                    this.setState({ loading: false});
                });
        }
    }
    startSearch(text){
        if(this.state.text === '') {
            Alert.alert('', 'عبارت مورد جستجو نمی تواند خالی باشد')
        }
        else {
            this.setState({ loading: true});
            const result = this.state.list.filter(item => item.title.includes(this.state.text));
            console.log('result', result)
            this.setState({loading: false, search: true, result: result});
        }
    }
    render() {
        if(this.state.loading)
            return (<Loader/>)
        else return(
                <View style={styles.container}>
                <View style={styles.header}>
                    <View style={styles.top}>
                        <Icon name="bell" size={20} color="black" />
                        <View style={{width: '80%',  position: 'relative', zIndex: 1}}>
                            <TextInput
                                maxLength={15}
                                placeholder={'جستجو در ویترین ها'}
                                placeholderTextColor={'gray'}
                                underlineColorAndroid='transparent'
                                value={this.state.text}
                                style={{
                                    height: 35,
                                    width: '90%',
                                    color: 'gray',
                                    fontSize: 14,
                                    textAlign: 'right',
                                    borderRadius: 20,
                                    backgroundColor: 'rgb(239, 239, 239)',
                                    paddingRight: 10
                                }}
                                onChangeText={(text) => this.setState({text: text})}
                            />
                            <TouchableOpacity onPress={() => this.startSearch(this.state.text)} style={{position: 'absolute', zIndex: 9992, top: '13%', left: '3%'}}>
                                <FIcon name="search" size={20} color="gray"  />
                            </TouchableOpacity>
                        </View>
                        <TouchableOpacity onPress={() => Actions.drawerOpen()}>
                            <Icon name="align-right" size={20} color="black" />
                        </TouchableOpacity>
                    </View>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={{flexDirection: 'row', flexWrap: 'wrap', padding: 10, paddingBottom: 200}}>
                        {
                            this.state.search ? (
                                this.state.result.length !==0 ? this.state.result.map((item, index)=>{
                                    let baseImg = null;
                                    if(item.main_image.length === 0 ){
                                        baseImg = "http://behya.com/uploads/defaults/thumb_600-600_default.png";
                                    }
                                    else {
                                        baseImg = "http://behya.com/uploads/vitrin_users/"+item.main_image[0].file
                                    }
                                    return <TouchableOpacity style={{borderRadius: 10}} key={index} onPress={() => Actions.vitrinDetail({item: item, baseImg: baseImg, loged: this.props.loged})}>
                                        <ImageBackground source={{uri: baseImg}} style={styles.subImage}>
                                            <View style={{backgroundColor:'rgba(0,0,0,.4)',
                                                height: 120,
                                                width: "100%",
                                                alignItems: "center",
                                                justifyContent: "center",
                                                padding: 20,
                                                borderRadius: 10
                                            }}>
                                                <Text style={styles.imageText}>{item.title}</Text>
                                            </View>
                                        </ImageBackground>
                                    </TouchableOpacity>
                                }) : <Text style={[styles.advLabel, {paddingLeft: '35%'}]}>موردی یافت نشد</Text>
                            ) :

                            this.state.list.length !==0 ? this.state.list.map((item, index)=>{
                                let baseImg = null;
                                if(item.main_image.length === 0 ){
                                    baseImg = "http://behya.com/uploads/defaults/thumb_600-600_default.png";
                                }
                                else {
                                    baseImg = "http://behya.com/uploads/vitrin_users/"+item.main_image[0].file
                                }
                                    return <TouchableOpacity style={{borderRadius: 10}} key={index} onPress={() => Actions.vitrinDetail({item: item, baseImg: baseImg, loged: this.props.loged})}>
                                        <ImageBackground source={{uri: baseImg}} style={styles.subImage}>
                                            <View style={{
                                                backgroundColor:'rgba(0,0,0,.4)',
                                                height: 120,
                                                width: "100%",
                                                alignItems: "center",
                                                justifyContent: "center",
                                                padding: 20,
                                                borderRadius: 10
                                            }}>
                                                <Text style={styles.imageText}>{item.title}</Text>
                                            </View>
                                        </ImageBackground>
                                    </TouchableOpacity>
                            }) : <Text style={styles.advLabel}>ویترینی برای نمایش وجود ندارد</Text>
                        }
                    </View>
                </ScrollView>
            </View>
        );
    }
}
export default Vitrin;
