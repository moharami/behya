import { StyleSheet, Dimensions } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(251, 251, 251)',
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'white',
        height: 70,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999
    },

    top: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'white',
        height: 60,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999,
        borderBottomColor: 'lightgray',
        borderBottomWidth: 1,
    },
    titleContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingRight: 30,
    },
    title: {
        paddingBottom: 15,
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 20,
        color: 'black',
        paddingRight: 15
    },
    body: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingLeft: 15,
        paddingRight: 15,
        marginBottom: 20
    },
    bodyContainer: {
        paddingBottom: 270,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: '3%',
        marginRight: '3%'
    },
    filtering: {
        position: 'absolute',
        bottom: 25,
        width: '60%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 15,
        paddingLeft: 25,
        backgroundColor: 'rgb(231, 21, 56)',
        borderRadius: 30,

    },
    scroll: {
        paddingTop: 80
    },
    category: {
        marginBottom: 30,
    },
    mainContainer: {
        flex: 1,
        flexDirection: 'row',
    },
    filterContainer: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonTitle: {
        fontSize: 15,
        color: 'lightgray'
    },
    mainImageContainer: {
        width: "100%",
        height: 170,
    },
    mainImage:{
        width: 140,
        height: 170,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomRightRadius: 5,
        borderTopRightRadius: 5,
        marginRight: 10,
        overflow: 'hidden'
    },
    imagesContainer: {
        flex: 1,
        flexDirection: 'column',
        width: "50%",
        alignItems: 'center',
        justifyContent: 'space-between',
        height: 170,
        borderRadius: 10,
        marginRight: 10
    },
    subImage: {
        // width: 160,
        width: Dimensions.get('screen').height*.25,
        height: 120,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        overflow: 'hidden',
        margin: 5
    },
    imageText: {
        backgroundColor: "transparent",
        fontSize: 14,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: "white",
        textAlign: 'center'

    },
    advLabel: {
        fontSize: 13,
        color: 'rgb(70, 70, 70)',
        paddingLeft: '23%',
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingTop: 35,
        alignSelf: 'center'
    }
});
