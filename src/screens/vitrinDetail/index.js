import React, {Component} from 'react';
import {ScrollView, View, ImageBackground, Alert, Text, TouchableOpacity, Image} from 'react-native';
import {connect} from 'react-redux';
import Loader from '../../components/loader/index'
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import AppSearchBar from '../../components/searchBar/index'
import Axios from 'axios'
import url from '../../config/address'
Axios.defaults.baseURL = url;
import FIcon from 'react-native-vector-icons/dist/Feather';
import Advertise from '../../components/advertise'

class Vitrin extends Component {
    constructor(props){
        super(props);
        this.state = {
            list: [],
            loading: false,
            active: '',
            result: [],
            search: false,
            text: ''
        };
    }
    componentWillMount() {
        this.setState({loading: true });
        Axios.get('/vitrin_list'
        ).then(response=> {
            this.setState({loading: false, list: response.data});
        })
            .catch((error) => {
                this.setState({ loading: false});
            });
    }
    startSearch(text){
        if(this.state.text === '') {
            Alert.alert('', 'عبارت مورد جستجو نمی تواند خالی باشد')
        }
        else {
            this.setState({ loading: true});
            const result = this.state.list.filter(item => item.title.includes(this.state.text));
            console.log('result', result)
            this.setState({loading: false, search: true, result: result});
        }
    }
    render() {
        console.log('this.props.item.ads.length !==0', this.props.item.ads.length !==0)
        console.log('this.props.item.ads', this.props.item.ads)
        if(this.state.loading)
            return (<Loader/>)
        else return(
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={styles.top}>
                        <Icon name="bell" size={20} color="black" />
                        <Text style={styles.titleHeader}>نمایش ویترین</Text>
                        <TouchableOpacity onPress={() => Actions.drawerOpen()}>
                            <Icon name="align-right" size={20} color="black" />
                        </TouchableOpacity>
                    </View>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={{width: '100%', paddingBottom: 200}}>
                        {
                            this.props.loged ?
                                <View style={styles.btnContainer}>
                                    <TouchableOpacity onPress={()=> Actions.addVitrin({item: this.props.item, edit: true})} style={[styles.btnItem, {backgroundColor: '#9b59b6', marginRight: 5}]}>
                                        <Text style={styles.btnLabel}>ویرایش ویترین</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={()=> Actions.composeAdvertise({vitrin: this.props.item.vitrin_id})} style={[styles.btnItem, {backgroundColor: '#5bc0de', marginLeft: 5}]}>
                                        <Text style={styles.btnLabel}>افزودن آگهی به ویترین</Text>
                                    </TouchableOpacity>
                                </View>
                                : null
                        }
                        <ImageBackground source={{uri: this.props.baseImg}} style={styles.subImage}/>
                        <Text style={styles.content}>{this.props.item.text}</Text>
                        <View style={[styles.body]}>
                            {
                                this.props.item.ads.length !==0 ?
                                this.props.item.ads.map((item, index) => <Advertise key={index} title={item.title} item={item} />) : null
                            }
                            {
                                this.props.item.ads.length !==0 ?
                                    this.props.item.ads.map((item, index) => <Advertise key={index} title={item.title} item={item} />) : null
                            }
                            {
                                this.props.item.ads.length !==0 ?
                                    this.props.item.ads.map((item, index) => <Advertise key={index} title={item.title} item={item} />) : null
                            }
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}
export default Vitrin;
