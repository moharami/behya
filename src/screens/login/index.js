import React, {Component} from 'react';
import { Text, View, BackHandler, TextInput, AsyncStorage,TouchableOpacity, Image, Alert} from 'react-native';
import styles from './styles'
import Loader from '../../components/loader'
import {Actions} from 'react-native-router-flux'
import logo from '../../assets/logo.jpg'
import LoginModal from './loginModal'
import SignupModal from './signupModal'
import {store} from '../../config/store';

class Login extends Component {
    constructor(props){
        super(props);
        this.state = {
            email: '',
            password: '',
            login: true,
            loading: false,
            loginModal: false,
            signupModal: false,
            logout: false
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    onBackPress(){
        this.logout();
        return true;
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        AsyncStorage.getItem('token').then((info) => {
            if(info !== null) {
                // this.setState({login: false});
                store.dispatch({type: 'USER_LOGED', payload: true});
            }
        });
    }
    logout() {
        Alert.alert(
            'خروج از حساب کاربری',
            'آیا از  خروج خود مطمئن هستید؟',
            [
                {text: 'خیر', onPress: () => this.setState({logout: false}), style: 'cancel'},
                {text: 'بله', onPress: () =>  this.setState({logout: true}, () => {BackHandler.exitApp()})},
            ]
        );
    }
    componentWillUpdate() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    closeModal() {
        this.setState({loginModal: false});
    }

    setModalVisible() {
        this.setState({loginModal: !this.state.loginModal});
    }
    closeModalSignup() {
        this.setState({signupModal: false});
    }

    setModalVisibleSignup() {
        this.setState({signupModal: !this.state.signupModal});
    }
    render() {
       return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Image style={styles.image} source={logo} />
                    <Text style={styles.logoTitle}>بخرید ، بفروشید ، معرفی کنید</Text>
                </View>
                <View style={styles.body}>
                    <Text  style={styles.title}>گسترده ترین سامانه ثبت آگهی</Text>
                    <View style={styles.redLine} />
                    <Text style={styles.subTitle}> عضویت فقط در ۱ دقیقه !</Text>
                    <TouchableOpacity onPress={() => Actions.home()} style={styles.advertise}>
                        <Text style={styles.buttonTitle}>مشاهده آگهی ها</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.setState({signupModal: true})} style={styles.membership}>
                        <Text style={styles.buttonTitle}>ثبت نام کنید</Text>
                    </TouchableOpacity>
                    <View style={styles.hr} />
                    <TouchableOpacity onPress={() => this.setState({loginModal: true})} style={styles.login}>
                        <Text style={styles.buttonTitle}>وارد شوید</Text>
                    </TouchableOpacity>
                    {/*<Text style={styles.footerTitle}>بازیابی رمز عبور</Text>*/}
                </View>
                <LoginModal
                    closeModal={() => this.closeModal()}
                    modalVisible={this.state.loginModal}
                    onChange={(visible) => this.setModalVisible(visible)}
                />
                <SignupModal
                    closeModal={() => this.closeModalSignup()}
                    modalVisible={this.state.signupModal}
                    onChange={(visible) => this.setModalVisibleSignup(visible)}
                />
            </View>
        );
    }
}
export default Login;