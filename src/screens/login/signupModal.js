
import React, { Component } from 'react';
import {
    Modal,
    Text,
    View,
    StyleSheet,
    TextInput,
    TouchableOpacity,
    AsyncStorage,
    Image,
    Alert
}
    from 'react-native'
import Icon from 'react-native-vector-icons/dist/FontAwesome'
import SelectFilter from '../../components/selectFilter'
import Axios from 'axios'
import url from '../../config/address'
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import {Actions} from 'react-native-router-flux'
import {
    SkypeIndicator,
} from 'react-native-indicators';

class SignupModal extends Component {
    state = {
        loading: false,
        email: '',
        password: '',
        mobile: ''
    };
    signup() {
        if(this.state.email === '' || this.state.password === '' || this.state.mobile === ''){
            Alert.alert('','لطفا تمام موارد را تکمیل فرمایید');
        }
        else if(this.state.password.length < 6){
            Alert.alert('','گذر واژه حداقل باید 6 رقمی باشد');
        }
        else if(this.state.mobile.length !== 11){
            Alert.alert('','لطفا شماره موبایل را صحیح وارد نمایید');
        }
        else {
            const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            if (reg.test(this.state.email) === true){
                this.setState({loading: true});
                Axios.post('/register', {
                    email: this.state.email,
                    password: this.state.password,
                    mobile: this.state.mobile
                }).then(response=> {
                    console.log('signup response', response.data)
                    if(response.data === 'OK') {
                        this.setState({loading: false});
                        Alert.alert('','ثبت نام شما با موفقیت انجام شد');
                        this.props.closeModal();
                    }
                    else {
                        this.setState({loading: false});
                        Alert.alert('', 'این ایمیل قبلا در سیستم ثبت شده است');
                        this.props.closeModal();
                    }
                })
                    .catch((error) => {
                        console.log(error.response)
                        Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                        this.setState({loadingMore: false});
                    });
            }
            else {
                Alert.alert('','لطفا ایمیل را صحیح وارد کنید');
            }
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <Modal animationType = {"fade"} transparent = {true}
                       visible = {this.props.modalVisible}
                       onRequestClose = {() => this.props.onChange(false)}
                       onBackdropPress= {() => this.props.onChange(!this.state.modalVisible)}
                >
                    <View style={styles.modal}>
                        <View style={styles.box}>
                            <View style={styles.headerContainer}>
                                <TouchableOpacity onPress={() => this.props.closeModal()}>
                                    <Icon name="close" size={20} color={ "black"} />
                                </TouchableOpacity>
                                <Text style={styles.headerText}>عضویت در بهیا</Text>
                            </View>
                            <TextInput
                                placeholder="ایمیل"
                                underlineColorAndroid='transparent'
                                value={this.state.email}
                                style={{
                                    height: 40,
                                    backgroundColor: 'white',
                                    paddingRight: 15,
                                    width: '95%',
                                    borderRadius: 10,
                                    borderWidth: 1,
                                    borderColor: 'lightgray',
                                    textAlign: 'right',
                                    fontFamily: 'IRANSansMobile(FaNum)',
                                    marginBottom: 20
                                }}
                                onChangeText={(text) => this.setState({email: text})} />
                            <TextInput
                                placeholder="موبایل"
                                underlineColorAndroid='transparent'
                                keyboardType={'numeric'}
                                value={this.state.mobile}
                                style={{
                                    height: 40,
                                    backgroundColor: 'white',
                                    paddingRight: 15,
                                    width: '95%',
                                    borderRadius: 10,
                                    borderWidth: 1,
                                    borderColor: 'lightgray',
                                    textAlign: 'right',
                                    fontFamily: 'IRANSansMobile(FaNum)',
                                    marginBottom: 20
                                }}
                                onChangeText={(text) => this.setState({mobile: text})} />
                            <TextInput
                                placeholder="پسورد"
                                secureTextEntry
                                underlineColorAndroid='transparent'
                                value={this.state.password}
                                style={{
                                    height: 40,
                                    backgroundColor: 'white',
                                    paddingRight: 15,
                                    width: '95%',
                                    borderRadius: 10,
                                    borderWidth: 1,
                                    borderColor: 'lightgray',
                                    textAlign: 'right',
                                    fontFamily: 'IRANSansMobile(FaNum)'
                                }}
                                onChangeText={(text) => this.setState({password: text})} />
                            {
                                this.state.loading ?
                                    <View style={{width: 60, height: 60}}>
                                        <SkypeIndicator color='rgb(231, 20, 56)' size={40} style={{height: 90}} />
                                    </View>
                                    :
                                    <TouchableOpacity onPress={() => this.signup()} style={styles.advertise}>
                                        <Text style={styles.buttonTitle}>ثبت نام</Text>
                                    </TouchableOpacity>
                            }
                        </View>
                    </View>
                </Modal>
            </View>
        )
    }
}
export default SignupModal

const styles = StyleSheet.create ({
    container: {
        alignItems: 'center',
        // backgroundColor: 'white',
        padding: 10,
        position: 'absolute',
        bottom: -200
    },
    modal: {
        position: 'relative',
        zIndex: 0,
        flexGrow: 1,
        justifyContent: 'center',
        // flex: 1,
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,.6)',
        paddingRight: 20,
        paddingLeft: 20
    },
    text: {
        color: '#3f2949',
        marginTop: 10
    },
    box: {
        width: '100%',
        backgroundColor: 'rgb(218, 218, 218)',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        borderRadius: 15,
        paddingBottom: 20
    },
    input: {
        width: '100%',
        fontSize: 16,
        paddingTop: 0,
        textAlign: 'right',
    },
    searchButton: {
        width: '90%',
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgb(20, 122, 170)'
    },
    searchText:{
        color: 'white'
    },
    picker: {
        height:50,
        width: "100%",
        alignSelf: 'flex-end'
    },
    label: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'rgb(60, 60, 60)'
    },
    headerContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 8,
        borderBottomWidth: 1,
        borderBottomColor: 'rgb(237, 237, 237)',
        marginBottom: 20
    },
    headerText: {
        fontSize: 14,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black'
    },
    advertise: {
        width: '35%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30,
        backgroundColor: '#8dc63f',
        padding: 5,
        marginTop: 10
    },
    buttonTitle: {
        fontSize: 14,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    inputLabel: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        alignSelf: 'flex-end',
        paddingRight: '10%',
        paddingBottom: 5
    },
    insContainer: {
        width: '100%',
        flexDirection: 'row',
        flexWrap: 'wrap',
        // alignItems: 'center',
        // justifyContent: 'space-between',
        padding: 8
    },
    info: {
        width: '100%',
        // alignItems: 'center',
        // justifyContent: 'flex-end',
        paddingTop: 20,
        paddingBottom: 20,
        borderBottomColor: 'white',
        borderBottomWidth: 1
    },
    rowsContainer: {
        width: '70%'
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    itemContainer: {
        width: '48%'
    },
    title: {
        color: 'rgb(60, 60, 60)',
        fontSize: 11,
        fontFamily: 'IRANSansMobile(FaNum)',
        alignSelf: 'flex-end',
        paddingRight: 15,
        paddingBottom: 10,
    },
    imageContainer: {
        borderRadius: 15,
        padding: 7,
        backgroundColor: 'white',
        width: '20%',
        marginRight: 10,
        marginLeft: 10
    },
})