import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingTop: '10%'
    },
    image: {
        // width: '33%',
        // resizeMode: 'contain'
    },
    logoTitle: {
        color: 'rgb(134, 59, 164)',
        fontSize: 14,
    },
    header: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    redLine: {
        width: '7%',
        borderTopColor: 'rgb(172, 49, 80)',
        borderTopWidth: 2
    },
    body: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 30
    },
    title: {
       fontSize: 22,
        color: 'black',
        paddingBottom: 13
    },
    subTitle: {
        fontSize: 18,
        color: 'rgb(185, 185, 185)',
        paddingTop: 20,
        paddingBottom: 40
    },
    advertise: {
        width: '85%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30,
        backgroundColor: 'rgb(82, 123, 189)',
        padding: 10,
        elevation: 3
    },
    buttonTitle: {
        fontSize: 17,
        color: 'white',
    },
    membership: {
        width: '85%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30,
        backgroundColor: 'rgb(245, 108, 118)',
        padding: 10,
        marginTop: 15
    },
    login: {
        width: '85%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30,
        backgroundColor: 'rgb(152, 80, 182)',
        padding: 10
    },
    hr: {
        width: '100%',
        borderTopColor: 'lightgray',
        borderTopWidth: 1,
        marginTop: 20,
        marginBottom: 25
    },
    footerTitle: {
        fontSize: 16,
        color: 'rgb(185, 185, 185)',
        paddingTop: 20,
        paddingBottom: 10
    }
});
