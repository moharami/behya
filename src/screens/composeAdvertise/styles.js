import { StyleSheet, Dimensions } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(251, 251, 251)',
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        height: 60,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999
    },
    top: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: 'white',
        height: 60,
        position: 'absolute',
        top: 0,
        right: 15,
        left: 15,
        zIndex: 9999,
        borderBottomColor: 'lightgray',
        borderBottomWidth: 1,
    },
    searchContainer: {
        width: '75%',
    },
    navbar: {
        flexDirection: 'row',
        position: 'absolute',
        top: 55,
        right: 0,
        left: 0,
        zIndex: 9999,
        padding: 20
    },
    body: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        // paddingLeft: 15,
        // paddingRight: 15,
        marginBottom: 20
    },
    bodyContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingRight: 20,
        paddingLeft: 20,
        paddingBottom: 180
    },
    mapStyle: {
        width: '100%',
        height: Dimensions.get('screen').height*.3
    },
    close: {
        position: 'absolute',
        bottom: 30,
        left: 20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 10,
        paddingLeft: 25,
        // backgroundColor: 'rgb(231, 21, 56)',
        borderRadius: 30,
    },
    insert: {
        position: 'absolute',
        bottom: 30,
        right: 20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 10,
        paddingLeft: 15,
        // backgroundColor: 'rgb(231, 21, 56)',
        borderRadius: 30,
    },
    scroll: {
        paddingTop: 60,
    },
    filterContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonTitle: {
        fontSize: 15,
        paddingRight: 15,
        paddingLeft: 70,
        color: 'lightgray'
    },
    buttonTitleClose: {
        fontSize: 15,
        paddingRight: 5,
        paddingLeft: 10,
        color: 'lightgray'
    },
    advTitle: {
        fontSize: 25,
        color: 'rgb(50, 50, 50)',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    titleContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingTop: 20,
        paddingRight: 20,
        paddingLeft: 20,
    },
    featureContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingRight: 20,
        backgroundColor: 'white'
    },
    detailContentContainer: {
        backgroundColor: 'white',
        margin: 20,
        paddingRight: 10,
        paddingTop: 20,
        paddingBottom: 20,
        elevation: 4,
        borderRadius: 5,
        paddingLeft: 20
    },
    detailContent: {
        lineHeight: 21,
        fontSize: 14,
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    pickerContainer: {
        position: 'relative',
        marginTop: 15,
        backgroundColor: 'white',
        width: '100%',
        borderWidth: 1,
        borderColor: 'lightgray',
        borderRadius: 18,
        height: 35,
        zIndex: 9999,
        overflow: 'hidden'
    },
    categoryPicker: {
        position: 'absolute',
        top: 5,
        bottom: 0,
        right: 0,
        left: 10,
        zIndex: 1,
        height: 18,
        width: "100%",
        backgroundColor: 'white'
    },
    selectIcon: {
        position: 'absolute',
        left: 10,
        top: 7,
        zIndex: 9999
    },
    location: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 15,
        paddingRight: 15
    },
    townPicker: {
        position: 'absolute',
        top: 5,
        bottom: 0,
        right: 0,
        left: 15,
        zIndex: 1,
        height: 18,
        width: "100%",
        backgroundColor: 'white',
    },
    reginPicker: {
        position: 'absolute',
        top: 5,
        bottom: 0,
        right: 0,
        left: 15,
        zIndex: 1,
        height: 18,
        width: "100%",
        backgroundColor: 'white',
    },
    town: {
        position: 'relative',
        backgroundColor: 'white',
        width: '50%',
        borderWidth: 1,
        borderColor: 'lightgray',
        borderRadius: 18,
        height: 35,
        zIndex: 9999,
        overflow: 'hidden',
        marginLeft: 13
    },
    regin: {
        position: 'relative',
        backgroundColor: 'white',
        width: '50%',
        borderWidth: 1,
        borderColor: 'lightgray',
        borderRadius: 18,
        height: 35,
        zIndex: 9999,
        overflow: 'hidden',
        marginLeft: 13
    },
    price: {
        alignItems: 'center',
        justifyContent: 'space-between',
        borderWidth: 1,
        borderColor: 'lightgray',
        borderRadius: 18,
        paddingLeft: 25,
        paddingRight: 25,
        paddingTop: 4,
        paddingBottom: 4,
        backgroundColor: 'white'
    },
    priceText: {
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    moreInfo: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 65
    },
    ImageContainer: {
        width: 70,
        height: 70,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#CDDC39',
        marginRight: 10,
        marginTop: 10

    },
    Image: {
        width: 80,
        height: 80,
        borderRadius: 6,
        marginLeft: 13,
        marginBottom: 13,
        alignItems: 'center',
        justifyContent: 'center',
        overflow: 'hidden'
    },
    photoContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: 20,
        paddingRight: 30,
        flexWrap: 'wrap'
    },
    addImage: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 80,
        height: 80,
        backgroundColor: 'rgb(59, 140, 219)',
        borderRadius: 6,
        marginBottom: 13
    },
    button: {
        // width: '25%',
        justifyContent: "center",
        alignItems: "center",
        padding: 5,
        borderRightColor: 'white',
        borderRightWidth: 10,
        backgroundColor: 'rgb(244, 244, 244)',
        marginTop: 10,
        borderRadius: 5,
        paddingTop: 10,
        paddingBottom: 10
    },
    buttonText: {
        color: 'gray',
        fontSize: 12
    },
    addButtonContainer: {
        justifyContent: "center",
        alignItems: "center",
        padding: 20
    },
    addButton: {
        width: '100%',
        justifyContent: "center",
        alignItems: "center",
        padding: 5,
        backgroundColor: 'rgb(224, 224, 224)',

    },
    addText: {
        color: 'white',
        fontSize: 12,
        paddingTop: 10
    },
    label: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 14,
        color: 'rgb(80, 80, 80)',
        paddingLeft: 15

    },
    addVideo: {
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: 'rgb(59, 140, 219)',
        padding: 5,
        borderRadius: 5
    },
    addVideoText: {
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 14
    },
    videoRow: {
        flexDirection: 'row',
        justifyContent: "center",
        alignItems: "center",
        width: '100%',
        margin: 10,
        marginBottom: 15
    },
    title: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 16,
        color: 'black',
    }
});
