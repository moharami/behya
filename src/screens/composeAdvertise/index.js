import React, {Component} from 'react';
import {ScrollView, View, AsyncStorage, Alert,  Image, Text, TouchableOpacity, TextInput, Picker, ImageBackground} from 'react-native';
import {connect} from 'react-redux';
import Loader from '../../components/loader'
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import AppSearchBar from '../../components/searchBar'
import LinearGradient from 'react-native-linear-gradient';
import CircleCheckBox from 'react-native-circle-checkbox'
import ImagePicker from 'react-native-image-picker';
import Axios from 'axios'
import url from '../../config/address'
Axios.defaults.baseURL = url;
import SwitchButton from 'switch-button-react-native';
import VideoPlayer from 'react-native-video-controls';
import MapView, { Marker } from 'react-native-maps';
import marker2 from '../../assets/marker.png'
import SelectFilter from '../../components/selectFilter'

class AdvertiseDetail extends Component {
    constructor(props){
        super(props);
        this.state = {
            text: '',
            link: '',
            description: '',
            price: '',
            loading: false,
            subject: 0,
            subValue: 0,
            checked: 1,
            checked2: false,
            ImageSource: null,
            imageCount: 0,
            imageArray: [],
            addImage: false,
            imageData: [],
            markers: [],
            video: null,
            videoSelected: false,
            videoData: null,
            loadingCat: false,
            loadingCity: false,
            loadingState: false,
            subCats: [],
            cities: [],
            states: [],
            advItems: [],
            city: 0,
            town: 0,
            activeSwitch: 1,
            activeSwitch2: 1,
            color: 'rgb(227, 5, 26)',
            color2: 'rgb(227, 5, 26)',
            initialLat: 35.715298,
            initialLng: 51.404343,
            catInfo: [],
            lat: null,
            lng: null,
        };
    }
    componentWillMount() {
        this.setState({loadingState: true});
        Axios.get('/states'
        ).then(response=> {
            this.setState({loadingState: false, states: response.data});
        })
        .catch((error) => {
            this.setState({loadingState: false});
        });
    }
    getCities(id) {
        this.setState({loadingCity: true});
        Axios.get('/cities/'+id
        ).then(response=> {
            this.setState({loadingCity: false, cities: response.data});
        })
        .catch((error) => {
            this.setState({loadingCity: false});
        });
    }
    addPhoto() {
        // this.setState((prevState) => ({
        //     imageCount: ++prevState.imageCount,
        // }));
        let countArray = {id: this.state.imageCount + 1, src: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcThOIdTKoMjafB-VCxYOiLskBCad0GHS-VgQDisiCd7Y0q2j0luEA'};
        const newImageArray = this.state.imageArray;
        newImageArray.push(countArray);
        this.setState({
            imageCount: ++this.state.imageCount,
            imageArray: newImageArray
        })
    }
    selectPhotoTapped() {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true,

            }
        };

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };

                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };

                let countArray = {id: this.state.imageCount + 1, src: response.uri};
                const newImageArray =  this.state.imageArray;
                newImageArray.push(countArray);
                this.setState({
                    imageCount: ++this.state.imageCount,
                    imageArray: newImageArray
                })
                // let imgArray = this.state.imageArray;
                // let imgData = this.state.imageData;
                // imgArray[item.id-1].src = source;
                // imgData[item.id-1] = response.uri;
                //
                // this.setState({
                //     imageArray: imgArray,
                //     imageData: imgData
                // });
            }
        });
    }
    handlePress(e) {
        // Axios.get('http://maps.google.com/maps/api/geocode/json?address=' + 'turkey').then(res =>   console.log(res));
        this.setState({
            markers: [
                {
                    coordinate: e.nativeEvent.coordinate,
                }
            ],
            lat: e.nativeEvent.coordinate.latitude,
            lng: e.nativeEvent.coordinate.longitude
        })
    }
    selectVideoTapped() {
        const options = {
            mediaType: 'video',
            videoQuality: 'low',
            waitUntilSaved: 10000 ,
            quality: 1.0,
            title: 'Video Picker',
            takePhotoButtonTitle: 'Take Video...',
            storageOptions: {
                skipBackup: true
            }
        };
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);
            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                if(response){
                    // const merged = "file:/"+ response.path;
                    const source = { uri: response.uri};
                    this.setState({
                        video: source,
                        videoSelected: true,
                        videoData: response
                    });
                    console.log('adddress ', this.state.video);
                }else{

                    alert('no video selected');
                }
            }
        });
    }
    getSubcat(id) {
        Axios.defaults.baseURL = 'https://behya.com/api';
        // this.setState({loading: true});
        console.log('this.state.subject ', this.state.subject )
        Axios.get('/category/'+this.state.subject , {
        }).then(response=> {
            console.log('respons catinfo', response.data)
            this.setState({catInfo: response.data, loadingCat: true});

            let items = [];
            response.data.features !== undefined && response.data.features.map((item) => {
                items.push({key: item.id, value: null})
            })
            this.setState({advItems: items}, () => console.log('this.state.advItems in will mount', this.state.advItems))

            Axios.get('/categories/'+id
            ).then(response=> {
                this.setState({loading: false, loadingCat: false, subCats: response.data.childs});
            })
            .catch((error) => {
                this.setState({ loadingCat: false});
            });
        })
        .catch((error) => {
            console.log(error.response)
            Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
            // this.setState({loading: false});
        });

    }
    deleteImg(id) {
        const newImageArray = this.state.imageArray.filter(item => item.id !== id);
        this.setState({
            imageArray: newImageArray
        })
    }
    addAdvertise() {
        // const photoes = this.state.imageArray.map((item, index)=> item.src)
        // let features = this.state.advItems.map(item=> {
        //     if(item.value !== null){
        //        return item.value
        //     }
        // })
        // features = features.filter(item=> item !== undefined)
        // console.log('features', features)
        // this.setState({loading: true});
        // Axios.post('/new_add', {
        //     marker_lat: this.state.initialLat,
        //     marker_lng: this.state.initialLng,
        //     city: this.state.city,
        //     vitrin: this.props.vitrin ? this.props.vitrin : null,
        //     list_price: this.state.price !== '' ? this.state.price : (this.state.checked === 2 ? -22: this.state.checked === 3 ? -21 : null),
        //     price: this.state.price !== '' ? this.state.price : (this.state.checked === 2 ? -22: this.state.checked === 3 ? -21 : null),
        //     secondaryCategory: this.state.subValue,
        //     features: features,
        //     photo: photoes.length !== 0 ? photoes : [],
        //     video: this.state.video!== null ? this.state.video : null,
        //     title: this.state.text,
        //     content: this.state.description,
        //     featured_link: this.state.link,
        //     user: ''
        // }).then(response=> {
        //     this.setState({ loading: false});
        // })
        // .catch((error) => {
        //     console.log(error.response)
        //     Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
        //     this.setState({loading: false});
        // });



        console.log("marker_lat", this.state.lat)
        console.log("marker_lng", this.state.lng)
        console.log("title", this.state.text)
        console.log("description", this.state.description)
        console.log("vitrin", this.props.vitrin ? this.props.vitrin : '')
        console.log("city", this.state.city)
        console.log("list_price", this.state.price !== '' ? this.state.price : (this.state.checked === 2 ? -22: this.state.checked === 3 ? -21 : null))
        console.log("price", this.state.price !== '' ? this.state.price : (this.state.checked === 2 ? -22: this.state.checked === 3 ? -21 : null))
        console.log("secondaryCategory", this.state.subValue)
        console.log("featured_link", this.state.link)
        console.log("featured", this.state.activeSwitch === 1 ||(this.state.video === null && this.state.link === '')  ? 0 : 1)
        console.log("mainCategory", this.state.subject)







        this.setState({loading: true});
        let features = [];
        const photoes = this.state.imageArray.map((item, index)=> item.src)
        features = this.state.advItems.map(item=> {
            if(item.value !== null){
                return item.value
            }
        })
        features = features.filter(item=> item !== undefined)
        console.log('features', features)
        let featureStr = "";
        features.map((item, index)=> {
            if(index === item.length){
                console.log('index', index)
                featureStr += item
            }
            else {
                featureStr += item+",";
            }
        });
        AsyncStorage.getItem('token').then((info) => {
            if(info !== null) {
                const newInfo = JSON.parse(info);
                let formdata = new FormData();
                formdata.append("marker_lat", this.state.lat)
                formdata.append("marker_lng", this.state.lng)
                formdata.append("title", this.state.text)
                formdata.append("description", this.state.description)
                formdata.append("user", newInfo.token)
                formdata.append("vitrin", this.props.vitrin ? this.props.vitrin : null)
                // formdata.append("vitrin", 9)
                formdata.append("city", this.state.city)
                formdata.append("list_price", this.state.price !== '' ? this.state.price : (this.state.checked === 2 ? -22: this.state.checked === 3 ? -21 : null))
                formdata.append("price", this.state.price !== '' ? this.state.price : (this.state.checked === 2 ? -22: this.state.checked === 3 ? -21 : null))
                formdata.append("secondaryCategory", this.state.subValue)
                formdata.append("secondary_cat", this.state.subValue)
                formdata.append("featured_link", this.state.link)
                formdata.append("features", featureStr)
                formdata.append("featured", this.state.activeSwitch === 1 ||(this.state.video === null && this.state.link === '')  ? 0 : 1)
                formdata.append("mainCategory", this.state.subject)

                console.log("user", newInfo.token)
                console.log("features[]", featureStr)

                photoes.length !== 0 && photoes.map((item)=> {
                    formdata.append("type", "image");
                    formdata.append("file", {
                        uri: item,
                        type: 'image/jpeg',
                        name: 'file'
                    });
                })
                // Axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';



                if(this.state.videoSelected){
                    console.log('videoSelected', this.state.videoSelected)
                    // formdata.append("type", "video");
                    // formdata.append("video",
                    //     {
                    //         uri: this.state.videoData.uri,
                    //         name: 'video/mp4',
                    //         type: 'file',
                    //     }
                    // )
                    formdata.append('video', {
                        uri: this.state.videoData.uri,
                        type: 'video/quicktime',
                        name: 'video.mov',
                    });
                }
                Axios.post('/new_add', formdata
                ).then(response=> {
                    console.log('response response',response.data)

                    Alert.alert('','آگهی با موفقیت افزوده شد');
                    this.setState({loading: false});
                })
                    .catch((response) => {
                        console.log('response response',response.response)
                        Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                        this.setState({loading: false});
                    });
                    console.log('videoSelected', this.state.videoSelected)
                    console.log('formdata', formdata)
            }
            else {
                Alert.alert('','لطفا وارد شوید');
                this.setState({loading: false});
            }
        });


    }
    changeFilter(id, fId) {
        console.log('this.state.advItems advitems', this.state.advItems)
        let newadvItems = this.state.advItems;

        this.state.catInfo.features.map((item) => {
            if(item.id === fId) {
                newadvItems = newadvItems.map((item)=> {
                    if(item.key === fId){
                        item.value = id
                    }
                })
            }
        })
        // this.setState({advItems: newadvItems})
        console.log('newadvItems', this.state.advItems)
    }
    render() {
        const {cats} = this.props;

        const info = Object.keys(this.state.catInfo).length !== 0 && this.state.catInfo.features.map((item, index) => {
            let feature = [{key: 0, label: item.title, value: 0}]
            item.variants.map((item) => {
                feature.push({key: item.id, label: item.title , value: item.id})
            })
            return <View key={index} style={{alignItems:'center', justifyContent: 'center'}}>
                {/*<Text style={styles.title}>{item.title}</Text>*/}
                <SelectFilter changeFilter={(id, fId) => this.changeFilter(id, fId)} items={feature} fId={item.id} label={item.title} compose/>
            </View>
        })

        if(this.state.loading)
            return (<Loader/>)
        else return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={styles.top}>
                        <Icon name="bell" size={20} color="black" />
                        {/*<AppSearchBar  placeholder="جستجو"/>*/}
                        <Text style={styles.title}>افزودن آگهی</Text>
                        <TouchableOpacity onPress={() => Actions.drawerOpen()}>
                            <Icon name="align-right" size={20} color="black" />
                        </TouchableOpacity>
                    </View>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.photoContainer}>
                        <TouchableOpacity onPress={() => this.selectPhotoTapped()}>
                            <View style={styles.addImage}>
                                <Icon name="camera" size={20} color="white"/>
                                <Text style={styles.addText}>افزودن عکس</Text>
                            </View>
                        </TouchableOpacity>
                        {this.state.imageCount !== 0 ?
                            this.state.imageArray.map((item, index) => {return <ImageBackground style={styles.Image} source={{uri: item.src}} key={index}>
                                    <View style={{
                                        backgroundColor:'rgba(0,0,0,.6)',
                                        height: 40,
                                        width: 40,
                                        alignItems: "center",
                                        justifyContent: "center",
                                        borderRadius: 30
                                    }}>
                                        <TouchableOpacity onPress={() => this.deleteImg(item.id)}>
                                            <Icon name="close" size={20} color="white" />
                                        </TouchableOpacity>
                                    </View>
                                </ImageBackground>
                                // <Image style={styles.Image} source={{uri: item.src}} key={index}/>
                            }) : null
                        }
                    </View>
                    <View style={styles.bodyContainer}>
                        <TextInput
                            placeholder="عنوان آگهی"
                            underlineColorAndroid='transparent'
                            value={this.state.text}
                            style={{
                                height: 40,
                                backgroundColor: 'white',
                                paddingRight: 15,
                                width: '100%',
                                borderRadius: 20,
                                borderWidth: 1,
                                borderColor: 'lightgray',
                                textAlign: 'right',
                                fontFamily: 'IRANSansMobile(FaNum)'
                            }}
                            onChangeText={(text) => this.setState({text})} />
                        <View style={styles.location}>
                            {/*<View style={styles.price}>*/}
                            {/*<Text style={styles.priceText}>190,000,000</Text>*/}
                            {/*</View>*/}
                            <View style={styles.town}>
                                <Icon name="angle-down" size={20} color="black" style={styles.selectIcon} />
                                <Picker
                                    mode="dropdown"
                                    style={styles.townPicker}
                                    selectedValue={this.state.city}
                                    onValueChange={itemValue => this.setState({ city: itemValue })}>
                                    <Picker.Item  label={this.state.loadingCity ? 'در حال بارگذاری' : "شهر"} value={0} />
                                    {
                                    this.state.cities.map((item, index)=> <Picker.Item key={index} label={item.title} value={item.id} />)
                                    }
                                </Picker>
                            </View>
                            <View style={styles.regin}>
                                <Icon name="angle-down" size={20} color="black" style={styles.selectIcon} />
                                <Picker
                                    mode="dropdown"
                                    style={styles.townPicker}
                                    selectedValue={this.state.town}
                                    onValueChange={itemValue => this.setState({ town: itemValue }, ()=> {this.getCities(itemValue)})}>
                                    <Picker.Item  label={this.state.loadingState ? 'در حال بارگذاری' : "استان"} value={0} />
                                    {
                                        this.state.states.map((item, index)=> <Picker.Item key={index} label={item.title} value={item.id} />
                                        )
                                    }
                                </Picker>
                            </View>
                        </View>

                        <View style={styles.location}>
                            {/*<View style={styles.price}>*/}
                                {/*<Text style={styles.priceText}>190,000,000</Text>*/}
                            {/*</View>*/}
                            <View style={styles.regin}>
                                <Icon name="angle-down" size={20} color="black" style={styles.selectIcon} />
                                <Picker
                                    mode="dropdown"
                                    style={styles.reginPicker}
                                    selectedValue={this.state.subValue}
                                    onValueChange={itemValue => this.setState({ subValue: itemValue })}>
                                    <Picker.Item  label={this.state.loadingCat ? 'در حال بارگذاری' : "زیر شاخه"} value={0} />
                                    {
                                        this.state.subCats.map((item, index)=> <Picker.Item key={index} label={item.title} value={item.id} />
                                        )
                                    }
                                </Picker>
                            </View>
                            <View style={styles.town}>
                                <Icon name="angle-down" size={20} color="black" style={styles.selectIcon} />
                                <Picker
                                    mode="dropdown"
                                    style={styles.categoryPicker}
                                    selectedValue={this.state.subject}
                                    onValueChange={itemValue => this.setState({ subject: itemValue }, ()=> {this.getSubcat(itemValue)})}>
                                    <Picker.Item  label="موضوع" value={0} />
                                    {
                                        cats.map((item, index)=> <Picker.Item key={index} label={item.title} value={item.id} />
                                        )
                                    }
                                </Picker>
                            </View>
                        </View>
                        <View style={{width: '100%', paddingTop: 15}}>
                            {info}
                        </View>
                        {/*<Text style={[styles.label, {fontSize: 13, paddingTop: 20, color: 'black', alignSelf:'flex-end'}]}>قیمت: </Text>*/}
                        <View style={[styles.location, {marginLeft: 15}]}>
                            <View style={{alignSelf: 'flex-end', marginRight: 10, marginBottom: 10}}>
                                <CircleCheckBox
                                    outerSize={15}
                                    innerSize={10}
                                    // innerColor=""
                                    label="توافقی"
                                    styleLabel={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 14, color: 'rgb(60, 60, 60)'}}
                                    labelPosition="left"
                                    outerColor="green"
                                    checked={this.state.checked === 2}
                                    onToggle={(checked) => this.setState({checked: 2, price: ''})}
                                />
                            </View>
                            <View style={{alignSelf: 'flex-end', marginRight: 10, marginBottom: 10}}>
                                <CircleCheckBox
                                    outerSize={15}
                                    innerSize={10}
                                    // innerColor=""
                                    label="تماس بگیرید"
                                    styleLabel={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 14, color: 'rgb(60, 60, 60)'}}
                                    labelPosition="left"
                                    outerColor="green"
                                    checked={this.state.checked === 3}
                                    onToggle={(checked) => this.setState({checked: 3, price: ''})}
                                />
                            </View>
                            <TextInput
                                placeholder="قیمت"
                                keyboardType={'numeric'}
                                underlineColorAndroid='transparent'
                                value={this.state.price}
                                style={{
                                    height: 34,
                                    backgroundColor: 'white',
                                    paddingRight: 15,
                                    width: '40%',
                                    borderRadius: 20,
                                    borderWidth: 1,
                                    borderColor: 'lightgray',
                                    textAlign: 'right',
                                    marginTop: 5,
                                    paddingTop: 6,
                                    marginLeft: 15,
                                    fontFamily: 'IRANSansMobile(FaNum)'
                                }}
                                onChangeText={(text) => this.setState({price: text, checked: 1})} />
                        </View>
                        <View style={{width: '100%'}}>
                            <Text style={[styles.label, {fontSize: 13, paddingBottom: 10, paddingTop: 20, color: 'black'}]}>توضیحات آگهی: </Text>
                            <TextInput
                                multiline = {true}
                                numberOfLines = {3}
                                value={this.state.description}
                                onChangeText={(text) => this.setState({description: text})}
                                placeholderTextColor={'rgb(142, 142, 142)'}
                                underlineColorAndroid='transparent'
                                placeholder="توضیحات..."
                                style={{
                                    // height: 45,
                                    paddingRight: 15,
                                    width: '100%',
                                    fontSize: 18,
                                    color: 'rgb(142, 142, 142)',
                                    borderWidth: 1,
                                    borderColor: 'lightgray',
                                    borderRadius: 10,
                                    textAlign: 'right',
                                    fontFamily: 'IRANSansMobile(FaNum)'
                                }}
                            />
                        </View>
                        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '100%', padding: 8, paddingTop: 20}}>
                            <SwitchButton
                                onValueChange={(val) => this.setState({ activeSwitch: val, color: this.state.color === 'rgb(227, 5, 26)' ? 'rgb(32, 193, 189)': 'rgb(227, 5, 26)' }, ()=> {console.log('active switch', this.state.activeSwitch)})}
                                text1 = ' '
                                text2 = ' '
                                switchWidth = {50}
                                switchHeight = {31}
                                switchdirection = 'ltr'
                                switchBorderRadius = {100}
                                switchSpeedChange = {500}
                                switchBorderColor = '#d4d4d4'
                                switchBackgroundColor = '#fff'
                                btnBorderColor = 'transparent'
                                btnBackgroundColor = {this.state.color}
                                fontColor = '#b1b1b1'
                                activeFontColor = '#fff'
                            />
                            <Text style={styles.label}>آگهی سفارشی(300 تومان ماهیانه): </Text>
                        </View>
                        {
                            this.state.activeSwitch === 1 ? null :
                                <View style={styles.videoRow}>
                                    <TouchableOpacity onPress={()=> this.setState({videoSelected: null})}>
                                        <View style={[styles.addVideo, {backgroundColor: 'rgb(245, 108, 118)', marginRight: 10}]}>
                                            <Text style={styles.addVideoText} >حذف ویدیو</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={this.selectVideoTapped.bind(this)}>
                                        <View style={styles.addVideo}>
                                            <Text style={styles.addVideoText} >آپلود ویدیو</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                        }
                        {this.state.videoSelected ? <VideoPlayer source={this.state.video} />: null}
                        {
                            this.state.activeSwitch === 1 ? null :
                            <View style={{width: '100%'}}>
                                <Text style={[styles.label, {fontSize: 13, paddingBottom: 10, paddingTop: 20}]}>لینک اختصاصی: (لطفا در ابتدای لینک خود //:http قرار دهید)</Text>
                                <TextInput
                                    placeholder="http://www.example.com"
                                    underlineColorAndroid='transparent'
                                    value={this.state.link}
                                    style={{
                                        height: 40,
                                        backgroundColor: 'white',
                                        paddingRight: 15,
                                        width: '100%',
                                        borderRadius: 20,
                                        borderWidth: 1,
                                        borderColor: 'lightgray',
                                        textAlign: 'right',
                                        fontFamily: 'IRANSansMobile(FaNum)'
                                    }}
                                    onChangeText={(text) => this.setState({link: text})} />
                            </View>
                        }
                        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '100%', padding: 8, paddingTop: 20}}>
                            <SwitchButton
                                onValueChange={(val) => this.setState({ activeSwitch2: val, color2: this.state.color2 === 'rgb(227, 5, 26)' ? 'rgb(32, 193, 189)': 'rgb(227, 5, 26)' }, ()=> {console.log('active switch', this.state.activeSwitch2)})}
                                text1 = ' '
                                text2 = ' '
                                switchWidth = {50}
                                switchHeight = {31}
                                switchdirection = 'ltr'
                                switchBorderRadius = {100}
                                switchSpeedChange = {500}
                                switchBorderColor = '#d4d4d4'
                                switchBackgroundColor = '#fff'
                                btnBorderColor = 'transparent'
                                btnBackgroundColor = {this.state.color2}
                                fontColor = '#b1b1b1'
                                activeFontColor = '#fff'
                            />
                            <Text style={styles.label}>مشاهده محل آگهی در نقشه: </Text>
                        </View>
                        {
                            this.state.activeSwitch2 === 1 ? null :
                            <MapView
                                style={styles.mapStyle}
                                region={{
                                    latitude: this.state.initialLat,
                                    longitude: this.state.initialLng,
                                    latitudeDelta: 0.0922,
                                    longitudeDelta: 0.0421,
                                }}
                                onPress={(event)=>this.handlePress(event)}
                            >
                                {this.state.markers.map((marker, index) => {
                                    return (
                                        <Marker key={index} {...marker} >
                                            <Image source={marker2} style={{width: 50, height: 50, resizeMode: 'contain'}} />
                                        </Marker>
                                    )
                                })}
                            </MapView>
                        }
                    </View>
                </ScrollView>
                {/*<View style={styles.filterContainer}>*/}
                    <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(250, 72, 72)', 'rgb(253, 109, 109)']} style={styles.close}>
                        <TouchableOpacity onPress={() => Actions.login()} style={{flexDirection: 'row'}}>
                            <Icon name="close" size={16} color="white" />
                            <Text style={styles.buttonTitleClose}>انصراف</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                    <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(148, 38, 251)', 'rgb(191, 49, 247 )']} style={styles.insert}>
                        <TouchableOpacity onPress={() => this.addAdvertise()} style={{flexDirection: 'row'}}>
                            <Icon name="check" size={20} color="white" />
                            <Text style={styles.buttonTitle}>ثبت آگهی</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                {/*</View>*/}
            </View>
        );
    }
}
function mapStateToProps(state) {
    return {
        posts: state.posts.posts,
        cats: state.posts.cats,
        advCats:  state.posts.advCats
    }
}
export default connect(mapStateToProps)(AdvertiseDetail);
