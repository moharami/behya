import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(251, 251, 251)',
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        height: 65,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999
    },
    top: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: 'white',
        height: 60,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999,
        borderBottomColor: 'lightgray',
        borderBottomWidth: 1,
        paddingRight:10,
        paddingLeft:10,
    },
    searchContainer: {
        width: '75%',
    },
    navbar: {
        flexDirection: 'row',
        position: 'absolute',
        top: 55,
        right: 0,
        left: 0,
        zIndex: 9999,
        padding: 20
    },
    body: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingLeft: 15,
        paddingRight: 15,
        marginBottom: 20
    },
    bodyContainer: {
        paddingBottom: 200,
    },
    chat: {
        position: 'absolute',
        bottom: 25,
        right: 30,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 10,
        paddingLeft: 15,
        // backgroundColor: 'rgb(231, 21, 56)',
        borderRadius: 30,
    },
    call: {
        position: 'absolute',
        bottom: 25,
        left: 30,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 10,
        paddingLeft: 15,
        // backgroundColor: 'rgb(231, 21, 56)',
        borderRadius: 30,
    },
    scroll: {
        paddingTop: 55,
        paddingBottom:205
    },
    filterContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonTitle: {
        fontSize: 15,
        paddingRight: 15,
        paddingLeft: 20,
        color: 'lightgray'
    },
    advTitle: {
        fontSize: 16,
        color: 'rgb(50, 50, 50)',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    titleContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingTop: 20,
        paddingRight: 20,
        paddingLeft: 10,
    },
    featureContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingRight: 20,
        backgroundColor: 'white'
    },
    detailContentContainer: {
        backgroundColor: 'white',
        margin: 20,
        paddingRight: 10,
        paddingTop: 20,
        paddingBottom: 20,
        elevation: 4,
        borderRadius: 5,
        paddingLeft: 20
    },
    detailContent: {
        lineHeight: 21,
        fontSize: 14,
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    mapContainer: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 100
    }
});
