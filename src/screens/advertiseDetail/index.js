import React, {Component} from 'react';
import {ScrollView, View, AsyncStorage, Image, Alert, BackHandler, Text, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import Loader from '../../components/loader'
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import AppSearchBar from '../../components/searchBar'
import FeatureItem from '../../components/featureItem'
import DetailNavbar from "../../components/detailNavbar";
import SwipeableParallaxCarousel from 'react-native-swipeable-parallax-carousel';
import SlideShow from "../../components/slideShow/index";
import LinearGradient from 'react-native-linear-gradient';
import MapView from 'react-native-maps';
import Axios from 'axios'
import url from '../../config/address'
Axios.defaults.baseURL = url;
import Communications from 'react-native-communications';

class AdvertiseDetail extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: true,
            detail: {},
            complete: false,
            initialLat: 35.715298,
            initialLng: 51.404343,
            login: false,
            user: null
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        AsyncStorage.getItem('token').then((info) => {
            if(info !== null) {
                const newInfo = JSON.parse(info);
                this.setState({login: true, user: newInfo.token});
            }
            else {
                this.setState({login: false});
            }
        });
        console.log('this.props.item.id', this.props.advId)
        Axios.post('/product/'+this.props.advId, {
        }).then(response=> {
            console.log('this is data',response)
            this.setState({loading: false, detail: response.data});
            this.state.detail.content && console.log('this.state.detail.content.length > 160',this.state.detail.content.slice(0, 160))

        })
            .catch((error) => {
                Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                this.setState({loading: false});
            });
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        console.log('back is press in blog detail')
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    render() {
        console.log('login', this.state.login);
        console.log('login', this.state.detail.user);


        console.log('this.state.detail.long', this.state.detail.long);
        console.log('this.state.detail.long', this.state.detail.lat);

        // const slider = [
        //     {illustration: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS6HMZshp7XCHTnDsoILrbw5nmPYTMGmC1O7tjENfAWgAlkw0zCHg'},
        //     {illustration: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS3o8Kl0tcSaY7W5zDJYiP0SScVExa2e5vgTnojRCwhVvOjVNRT'},
        //     {illustration: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS3o8Kl0tcSaY7W5zDJYiP0SScVExa2e5vgTnojRCwhVvOjVNRT'}
        // ]
        const slideItems = [
            {"id": 33996,"imagePath": 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRMNPqs2p9MSqWCIYLzpZdr7JOmWD5L7vTHk7UxSvn_zk7EqKZN'},
            {"id": 33964,"imagePath": 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTUXDh_gaK6AUkycJ2IBuLXdxKfN-GB4nwObSmKWEhztck-82-u'},
            {"id": 3364,"imagePath": 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTUXDh_gaK6AUkycJ2IBuLXdxKfN-GB4nwObSmKWEhztck-82-u'},

        ];
        if(this.state.loading)
            return (<Loader/>)
        else
            return (
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.top}>
                            <Icon name="bell" size={20} color="black" />
                            {/*<AppSearchBar />*/}
                            <TouchableOpacity onPress={() => Actions.drawerOpen()}>
                                <Icon name="align-right" size={20} color="black" />
                            </TouchableOpacity>
                        </View>
                        {/*<ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{paddingTop: 20, marginLeft: 10, marginRight: 10}}>*/}
                        {/*/!*<View style={styles.navbar}>*!/*/}
                        {/*<DetailNavbar bgColor="rgb(64, 134, 222)" icon="plus" label="افزودن فیلتر" space={true} />*/}
                        {/*<DetailNavbar bgColor="rgb(73, 221, 185)" icon="close" label="اتوماتیک" />*/}
                        {/*<DetailNavbar bgColor="rgb(73, 221, 185)" icon="close" label="خودرو" />*/}
                        {/*<DetailNavbar bgColor="rgb(73, 221, 185)" icon="close" label="خودرو" />*/}
                        {/*<DetailNavbar bgColor="rgb(73, 221, 185)" icon="close" label="خودرو" />*/}
                        {/*/!*</View>*!/*/}
                        {/*</ScrollView>*/}
                    </View>
                    <ScrollView style={styles.scroll}>
                        <View style={styles.titleContainer}>
                            <FeatureItem price label={this.state.detail.list_price === -22 ? 'تماس بگیرید' : (this.state.detail.list_price === -21 ? 'توافقی' : this.state.detail.list_price+ ' ریال')}/>
                            <Text style={[styles.advTitle, {fontSize: this.state.detail.title.length > 16 ? 13 : 23 }]}>{this.state.detail.title}</Text>
                        </View>
                        <View>
                            <SlideShow activeSlide={1} photos={this.state.detail.photos} />
                        </View>
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{paddingTop: 20, marginLeft: 10, marginRight: 10}}>
                            {/*{*/}
                            {/*this.state.detail.feature_variants.length !== 0 && this.state.detail.feature_variants.map((item, index)=> <FeatureItem key={index} price={false} label={item} />)*/}
                            {/*}*/}
                            {
                                this.state.detail.feature_variant ? <FeatureItem price={false} label={this.state.detail.feature_variants[0].title} /> : null
                            }
                        </ScrollView>

                        <View style={styles.detailContentContainer}>
                            {

                                this.state.detail.content && (this.state.detail.content.length > 160 ? (this.state.complete ?
                                        <Text style={styles.detailContent}>{this.state.detail.content}</Text> :
                                        <TouchableOpacity  onPress={() => this.setState({complete: !this.state.complete})} style={{flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'flex-end'}}>
                                            <Text style={styles.detailContent}>{this.state.detail.content.slice(0, 160) + '...'}</Text>
                                        </TouchableOpacity>
                                )
                                    :

                                    <Text style={styles.detailContent}>{this.state.detail.content}</Text>)

                            }
                        </View>
                        <View style={styles.mapContainer}>
                            <MapView
                                style={{width: '90%', height: 200}}
                                initialRegion={{
                                    latitude: this.state.detail.lat === 0 ||this.state.detail.lat === ''  ? 35.715298 : parseInt(this.state.detail.lat),
                                    longitude: this.state.detail.long === 0 ||this.state.detail.lat === ''  ? 51.404343 :parseInt(this.state.detail.long),
                                    latitudeDelta: 0.0922,
                                    longitudeDelta: 0.0421,
                                }}
                            />
                        </View>
                    </ScrollView>
                    <View style={styles.filterContainer}>
                        <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(148, 38, 251)', 'rgb(191, 49, 247 )']} style={styles.chat}>
                            <TouchableOpacity onPress={() => {this.state.login ? Actions.chat({id: this.state.detail.user.id, user: this.state.detail.user}): Alert.alert('', 'لطفا ابتدا وارد شوید')}} style={{flexDirection: 'row'}}>
                                <Icon name="comment" size={20} color="white" />
                                <Text style={styles.buttonTitle}>گفتگو</Text>
                            </TouchableOpacity>
                        </LinearGradient>
                        <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(148, 38, 251)', 'rgb(191, 49, 247 )']} style={styles.call}>
                            <TouchableOpacity onPress={() => Communications.phonecall(this.state.detail.user.mobile, true)} style={{flexDirection: 'row'}}>
                                <Icon name="phone" size={20} color="white" />
                                <Text style={styles.buttonTitle}>تماس با فروشنده</Text>
                            </TouchableOpacity>
                        </LinearGradient>
                        {/*<View style={styles.call}>*/}
                        {/*<Icon name="phone" size={24} color="white" />*/}
                        {/*<Text style={styles.buttonTitle}>تماس با فروشنده</Text>*/}
                        {/*</View>*/}
                    </View>
                </View>
            );
    }
}
export default AdvertiseDetail;