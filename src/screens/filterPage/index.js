import React, {Component} from 'react';
import {ScrollView, View, Text, Alert, TextInput, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import Loader from '../../components/loader'
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import FIcon from 'react-native-vector-icons/dist/Feather';
import NavbarItem from '../../components/navbarItem'
import Advertise from "../../components/advertise/index";
import AppSearchBar from '../../components/searchBar'
import FeatureItem from '../../components/featureItem'
import Axios from 'axios'
import url from '../../config/address'
Axios.defaults.baseURL = url;

class FilterPage extends Component {
    constructor(props){
        super(props);
        this.state = {
            posts: [],
            loading: false,
            text: '',
            result: [],
            search: false
        };
    }
    startSearch(text){
        if(this.state.text === '') {
            Alert.alert('', 'عبارت مورد جستجو نمی تواند خالی باشد')
        }
        else {
            this.setState({ loading: true});
            const result = this.props.datafiltered.products.data.filter(item => item.title.includes(this.state.text));
            console.log('result', result)
            this.setState({loading: false, search: true, result: result});
        }
    }
    render() {
        console.log('datafiltered', this.props.datafiltered)
        if(this.state.loading)
            return (<Loader/>)
        else return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={styles.top}>
                        <Icon name="bell" size={20} color="black" />
                        <View style={{width: '80%',  position: 'relative', zIndex: 1}}>
                            <TextInput
                                maxLength={15}
                                placeholder={'جستجو ...'}
                                placeholderTextColor={'gray'}
                                underlineColorAndroid='transparent'
                                value={this.state.text}
                                style={{
                                    height: 35,
                                    width: '90%',
                                    color: 'gray',
                                    fontSize: 14,
                                    textAlign: 'right',
                                    borderRadius: 20,
                                    backgroundColor: 'rgb(239, 239, 239)',
                                    paddingRight: 10
                                }}
                                onChangeText={(text) => this.setState({text: text})}
                            />
                            <TouchableOpacity onPress={() => this.startSearch(this.state.text)} style={{position: 'absolute', zIndex: 9992, top: '13%', left: '3%'}}>
                                <FIcon name="search" size={20} color="gray"  />
                            </TouchableOpacity>
                        </View>
                        <TouchableOpacity onPress={() => Actions.drawerOpen()}>
                            <Icon name="align-right" size={20} color="black" />
                        </TouchableOpacity>
                    </View>
                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={styles.navbarScroll}>
                        <View style={styles.navbar}>
                            <NavbarItem id={2} bgColor="rgb(241, 220, 75)" icon="truck" label="" small />
                            {
                                this.props.datafiltered.selected_features.map((item, index)=>
                                {
                                    const label = item.feature.title +': '+ item.title;
                                    return <FeatureItem key={index} price={false} label={label} filter bgColor="rgb(239, 239, 239)"/>
                                })
                            }
                        </View>
                    </ScrollView>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.bodyContainer}>
                        <View style={styles.body}>
                            {
                                this.state.search ? this.state.result.length !== 0 ? this.state.result.map((item, index) => <Advertise key={index} title={item.title} item={item} />) :
                                    <Text style={[styles.advLabel, {paddingLeft: '35%'}]}>موردی یافت نشد</Text> :
                                this.props.datafiltered.products.data && this.props.datafiltered.products.data.map((item, index) => <Advertise key={index} title={item.title} item={item} />)
                            }
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}
export default FilterPage;
