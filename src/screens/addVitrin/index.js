import React, {Component} from 'react';
import {ScrollView, View, AsyncStorage, Alert,  Image, Text, TouchableOpacity, TextInput, Picker, ImageBackground} from 'react-native';
import {connect} from 'react-redux';
import Loader from '../../components/loader'
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import AppSearchBar from '../../components/searchBar'
import LinearGradient from 'react-native-linear-gradient';
import CircleCheckBox from 'react-native-circle-checkbox'
import ImagePicker from 'react-native-image-picker';
import Axios from 'axios'
import url from '../../config/address'
Axios.defaults.baseURL = url;
import SwitchButton from 'switch-button-react-native';
import VideoPlayer from 'react-native-video-controls';
import MapView, { Marker } from 'react-native-maps';
import marker2 from '../../assets/marker.png'
import SelectFilter from '../../components/selectFilter'
import {store} from '../../config/store';

class AdvertiseDetail extends Component {
    constructor(props){
        super(props);
        this.state = {
            text: this.props.edit ? this.props.item.title : '',
            description: this.props.edit ? this.props.item.text : '',
            loading: false,
            checked: 1,
            checked2: false,
            ImageSource: null,
            imageCount: 0,
            imageArray: [],
            addImage: false,
            imageData: [],
            markers: [],
            vitrins: [],
            city: 0,
            town: 0,
            activeSwitch: 1,
            activeSwitch2: 1,
            color: 'rgb(227, 5, 26)',
            color2: 'rgb(227, 5, 26)',
            initialLat:  this.props.edit ? (this.props.item.lat !== "" ? this.props.item.lat : 35.715298) : 35.715298,
            initialLng:  this.props.edit ? (this.props.item.long !== "" ? this.props.item.long : 51.404343)  : 51.404343,
            headerSrc: null,
            logoSrc: null
        };
    }
    selectPhotoTapped(id) {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true
            }
        };

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };

                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };
                console.log('id', id)
                if(id === 1) {
                    this.setState({headerSrc: response.uri}, () => {console.log(this.state.headerSrc)})
                }
                else if(id === 2) {
                    this.setState({logoSrc: response.uri}, () => {console.log(this.state.headerSrc)})
                }
            }
        });
    }
    handlePress(e) {
        // Axios.get('http://maps.google.com/maps/api/geocode/json?address=' + 'turkey').then(res =>   console.log(res));
        this.setState({
            markers: [
                {
                    coordinate: e.nativeEvent.coordinate,
                }
            ],
            lat: e.nativeEvent.coordinate.latitude,
            lng: e.nativeEvent.coordinate.longitude
        })
    }
    deleteImg(id) {
        const newImageArray = this.state.imageArray.filter(item => item.id !== id);
        this.setState({
            imageArray: newImageArray
        })
    }
    componentWillMount() {
        this.setState({ loading: true});
        Axios.get('/vitrins').then(response=> {
            // store.dispatch({type: 'VITRINS_FETCHED', payload: response.data});
            this.setState({ loading: false, vitrins: response.data});
            console.log('vitrins', this.state.vitrins)
        })
        .catch((error) => {
            console.log(error.response)
            Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
            this.setState({loading: false});
        });
    }
    addVitrin() {
        this.setState({loading: true});
        AsyncStorage.getItem('token').then((info) => {
            if(info !== null) {
                const newInfo = JSON.parse(info);

                let formdata = new FormData();
                formdata.append("lat", this.state.initialLat)
                formdata.append("long", this.state.initialLng)
                formdata.append("title", this.state.text)
                formdata.append("description", this.state.description)
                formdata.append("user", newInfo.token)
                formdata.append("package", this.state.activeVitrin)

                if(this.state.headerSrc !== null) {
                    formdata.append("type", "image");
                    formdata.append("file", {
                        uri: this.state.headerSrc,
                        type: 'image/jpeg',
                        name: 'file'
                    });
                }
                else if(this.state.logoSrc !== null) {
                    formdata.append("type", "image");
                    formdata.append("file", {
                        uri: this.state.logoSrc,
                        type: 'image/jpeg',
                        name: 'file'
                    });
                }
                Axios.post('/new_vitrin', formdata).then(response=> {
                    Alert.alert('','ویترین با موفقیت افزوده شد');
                    this.setState({ loading: false});
                })
                    .catch((error) => {
                        console.log(error.response)
                        Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                        this.setState({loading: false});
                    });
            }
            else {
                Alert.alert('','لطفا وارد شوید');
                this.setState({loading: false});
            }
        });
    }
    editvitrin() {
        this.setState({loading: true});
        AsyncStorage.getItem('token').then((info) => {
            if(info !== null) {
                const newInfo = JSON.parse(info);

                let formdata = new FormData();
                formdata.append("lat", this.state.initialLat)
                formdata.append("long", this.state.initialLng)
                formdata.append("title", this.state.text)
                formdata.append("description", this.state.description)
                formdata.append("user", newInfo.token)

                if(this.state.headerSrc !== null) {
                    formdata.append("type", "image");
                    formdata.append("file", {
                        uri: this.state.headerSrc,
                        type: 'image/jpeg',
                        name: 'file'
                    });
                }
                else if(this.state.logoSrc !== null) {
                    formdata.append("type", "image");
                    formdata.append("file", {
                        uri: this.state.logoSrc,
                        type: 'image/jpeg',
                        name: 'file'
                    });
                }
                Axios.post('/update_vitrin/'+this.props.item.id, formdata).then(response=> {
                    Alert.alert('','ویترین با موفقیت ویرایش شد');
                    this.setState({ loading: false});
                })
                    .catch((error) => {
                        console.log(error.response)
                        Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                        this.setState({loading: false});
                    });
            }
            else {
                Alert.alert('','لطفا وارد شوید');
                this.setState({loading: false});            }
        });
    }
    render() {
        const {cats} = this.props;
        // console.log('vitrins', vitrins)
        if(this.state.loading)
            return (<Loader/>)
        else return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={styles.top}>
                        <Icon name="bell" size={20} color="black" />
                        {/*<AppSearchBar  placeholder="جستجو"/>*/}
                        <Text style={styles.title}>{this.props.edit ? 'ویرایش' : 'ثبت'} ویترین {this.props.edit? "" : 'جدید'}</Text>
                        <TouchableOpacity onPress={() => Actions.drawerOpen()}>
                            <Icon name="align-right" size={20} color="black" />
                        </TouchableOpacity>
                    </View>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.photoContainer}>
                        {
                            this.state.headerSrc !== null ?
                                <ImageBackground style={styles.Image} source={{uri: this.state.headerSrc}}>
                                    <View style={{
                                        backgroundColor:'rgba(0,0,0,.6)',
                                        height: 40,
                                        width: 40,
                                        alignItems: "center",
                                        justifyContent: "center",
                                        borderRadius: 30
                                    }}>
                                        <TouchableOpacity onPress={() => this.setState({headerSrc: null})}>
                                            <Icon name="close" size={20} color="white" />
                                        </TouchableOpacity>
                                    </View>
                                </ImageBackground> :
                                <TouchableOpacity onPress={() => this.selectPhotoTapped(1)}>
                                    <View style={styles.addImage}>
                                        <Icon name="camera" size={20} color="white"/>
                                        <Text style={styles.addText}>هدر ویترین</Text>
                                    </View>
                                </TouchableOpacity>
                        }
                        {
                            this.state.logoSrc !== null ?
                                <ImageBackground style={styles.Image} source={{uri: this.state.logoSrc}}>
                                    <View style={{
                                        backgroundColor:'rgba(0,0,0,.6)',
                                        height: 40,
                                        width: 40,
                                        alignItems: "center",
                                        justifyContent: "center",
                                        borderRadius: 30
                                    }}>
                                        <TouchableOpacity onPress={() => this.setState({logoSrc: null})}>
                                            <Icon name="close" size={20} color="white" />
                                        </TouchableOpacity>
                                    </View>
                                </ImageBackground> :
                                <TouchableOpacity onPress={() => this.selectPhotoTapped(2)}>
                                    <View style={styles.addImage}>
                                        <Icon name="camera" size={20} color="white"/>
                                        <Text style={styles.addText}>لوگوی ویترین</Text>
                                    </View>
                                </TouchableOpacity>

                        }
                    </View>
                    <View style={styles.bodyContainer}>
                        <TextInput
                            placeholder="عنوان ویترین"
                            underlineColorAndroid='transparent'
                            value={this.state.text}
                            style={{
                                height: 40,
                                backgroundColor: 'white',
                                paddingRight: 15,
                                width: '100%',
                                borderRadius: 20,
                                borderWidth: 1,
                                borderColor: 'lightgray',
                                textAlign: 'right',
                                fontFamily: 'IRANSansMobile(FaNum)'
                            }}
                            onChangeText={(text) => this.setState({text})} />
                        {
                            this.props.edit ? null :
                                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{paddingTop: 20}}>
                                    {
                                        this.state.vitrins.length !==0 && this.state.vitrins.map((item, index) => <TouchableOpacity key={index} onPress={()=> this.setState({activeVitrin: item.id})} style={[styles.vitrin, {backgroundColor: this.state.activeVitrin === item.id ? '#d8bfd8' : 'white'}]}>
                                            <Text style={styles.vitrinCount}>{item.title}</Text>
                                            <Text style={styles.vitrinPrice}>{item.price} تومان</Text>
                                        </TouchableOpacity>)
                                    }
                                </ScrollView>
                        }
                        <View style={{width: '100%'}}>
                            <Text style={[styles.label, {fontSize: 13, paddingBottom: 10, paddingTop: 20, color: 'black'}]}>متن ویترین: </Text>
                            <TextInput
                                multiline = {true}
                                numberOfLines = {3}
                                value={this.state.description}
                                onChangeText={(text) => this.setState({description: text})}
                                placeholderTextColor={'rgb(142, 142, 142)'}
                                underlineColorAndroid='transparent'
                                placeholder="توضیحات"
                                style={{
                                    // height: 45,
                                    paddingRight: 15,
                                    width: '100%',
                                    fontSize: 14,
                                    color: 'rgb(142, 142, 142)',
                                    borderWidth: 1,
                                    borderColor: 'lightgray',
                                    borderRadius: 10,
                                    textAlign: 'right',
                                    fontFamily: 'IRANSansMobile(FaNum)'
                                }}
                            />
                        </View>
                        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '100%', padding: 8, paddingTop: 20}}>
                            <SwitchButton
                                onValueChange={(val) => this.setState({ activeSwitch2: val, color2: this.state.color2 === 'rgb(227, 5, 26)' ? 'rgb(32, 193, 189)': 'rgb(227, 5, 26)' }, ()=> {console.log('active switch2', this.state.activeSwitch2)})}
                                text1 = ' '
                                text2 = ' '
                                switchWidth = {50}
                                switchHeight = {31}
                                switchdirection = 'ltr'
                                switchBorderRadius = {100}
                                switchSpeedChange = {500}
                                switchBorderColor = '#d4d4d4'
                                switchBackgroundColor = '#fff'
                                btnBorderColor = 'transparent'
                                btnBackgroundColor = {this.state.color2}
                                fontColor = '#b1b1b1'
                                activeFontColor = '#fff'
                            />
                            <Text style={styles.label}>مشاهده محل آگهی در نقشه: </Text>
                        </View>
                        {
                            this.state.activeSwitch2 === 1 ? null :
                                <MapView
                                    style={styles.mapStyle}
                                    region={{
                                        latitude: this.state.initialLat,
                                        longitude: this.state.initialLng,
                                        latitudeDelta: 0.0922,
                                        longitudeDelta: 0.0421,
                                    }}
                                    onPress={this.handlePress}
                                >
                                    {this.state.markers.map((marker, index) => {
                                        return (
                                            <Marker key={index} {...marker} >
                                                <Image source={marker2} style={{width: 50, height: 50, resizeMode: 'contain'}} />
                                            </Marker>
                                        )
                                    })}
                                </MapView>
                        }
                    </View>
                </ScrollView>
                {/*<View style={styles.filterContainer}>*/}
                <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(250, 72, 72)', 'rgb(253, 109, 109)']} style={styles.close}>
                    <TouchableOpacity onPress={() => Actions.login()} style={{flexDirection: 'row'}}>
                        <Icon name="close" size={16} color="white" />
                        <Text style={styles.buttonTitleClose}>انصراف</Text>
                    </TouchableOpacity>
                </LinearGradient>
                <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(148, 38, 251)', 'rgb(191, 49, 247 )']} style={styles.insert}>
                    <TouchableOpacity onPress={() => this.props.edit ? this.editvitrin() : this.addVitrin()} style={{flexDirection: 'row'}}>
                        <Icon name="check" size={20} color="white" />
                        <Text style={styles.buttonTitle}>ثبت ویترین</Text>
                    </TouchableOpacity>
                </LinearGradient>
                {/*</View>*/}
            </View>
        );
    }
}
function mapStateToProps(state) {
    return {
        posts: state.posts.posts,
        cats: state.posts.cats,
        advCats: state.posts.advCats,
    }
}
export default connect(mapStateToProps)(AdvertiseDetail);
