
import React, { Component } from 'react';
import {
    Modal,
    Text,
    View,
    StyleSheet,
    TextInput,
    TouchableOpacity,
    AsyncStorage,
    Image,
    Alert
}
    from 'react-native'
import Icon from 'react-native-vector-icons/dist/FontAwesome'
import SelectFilter from '../../components/selectFilter'
import Axios from 'axios'
import url from '../../config/address'
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import {Actions} from 'react-native-router-flux'
import {
    SkypeIndicator,
} from 'react-native-indicators';

class FilterModal extends Component {
    state = {
      text: '',
        advItems: []
    };
    onDateChange(date) {
        setTimeout(() => {this.setState({showPicker: false})}, 200)
        this.setState({ selectedStartDate: date });
    }

    filterAdv() {
        console.log('this.state.advitems in filter adv', this.state.advItems)
        let selectedItm = []
        this.state.advItems.map((item) => {
            selectedItm.push(item.value)
        })
        let str = '';
        selectedItm.map((item, index)=>{
            str+= 'selected[]='+item;
            if(selectedItm.length !== index+1){
                str+='&'
            }
        })

        console.log('str', str)
        console.log('/category/this.props.catInfo.category.id+str', this.props.catInfo.category.id+'?'+str)
        this.setState({loading: true});
        Axios.get('/category/'+this.props.catInfo.category.id+'?'+str
        ).then(response=> {
            this.setState({loading: false});
            this.props.closeModal();
            Actions.filterPage({datafiltered: response.data})
            console.log('response', response.data)
        })
        .catch((error) => {
            Alert.alert('', 'خطایی رخ داده است مجددا تلاش نمایید')
            this.setState({loading: false});
        });
    }
    componentDidMount() {
        let items = [];
        this.props.catInfo.features !== undefined && this.props.catInfo.features.map((item) => {
            items.push({key: item.id, value: null})
        })
        this.setState({advItems: items}, () => console.log('this.state.advItems in will mount', this.state.advItems))
    }
    changeFilter(id, fId) {
        console.log('this.state.advItems advitems', this.state.advItems)
        let newadvItems = this.state.advItems;

        this.props.catInfo.features.map((item) => {
            if(item.id === fId) {
                newadvItems = newadvItems.map((item)=> {
                    if(item.key === fId){
                        item.value = id
                    }
                })
            }
        })
        // this.setState({advItems: newadvItems})
        console.log('newadvItems', this.state.advItems)
    }
    render() {
        const info = Object.keys(this.props.catInfo).length !== 0 && this.props.catInfo.features.map((item, index) => {
            let feature = [{key: 0, label: "انتخاب نشده", value: 0}]
            item.variants.map((item) => {
                feature.push({key: item.id, label: item.title , value: item.id})
            })
            return <View key={index} style={{alignItems:'center', justifyContent: 'center'}}>
                        <Text style={styles.title}>{item.title}</Text>
                        <SelectFilter changeFilter={(id, fId) => this.changeFilter(id, fId)} items={feature} fId={item.id} label={item.title}/>
                    </View>
        })
        return (
            <View style={styles.container}>
                <Modal animationType = {"fade"} transparent = {true}
                       visible = {this.props.modalVisible}
                       onRequestClose = {() => this.props.onChange(false)}
                       onBackdropPress= {() => this.props.onChange(!this.state.modalVisible)}
                >
                    <View style={styles.modal}>
                        <View style={styles.box}>
                            <View style={styles.headerContainer}>
                                <TouchableOpacity onPress={() => this.props.closeModal()}>
                                    <Icon name="close" size={20} color={ "black"} />
                                </TouchableOpacity>
                                <Text style={styles.headerText}>فیلتر</Text>
                            </View>
                            <View style={styles.info}>
                                {
                                    info.length !== 0 ? info : <Text style={styles.title}>فیلتری یافت نشد</Text>
                                }
                            </View>
                            {
                                this.state.loading ?
                                    <View style={{width: 60, height: 60}}>
                                        <SkypeIndicator color='rgb(231, 20, 56)' size={40} style={{height: 90}} />
                                    </View>
                                        :
                                    <TouchableOpacity onPress={() => this.filterAdv()} style={styles.advertise}>
                                        <Text style={styles.buttonTitle}>تایید</Text>
                                    </TouchableOpacity>
                            }
                        </View>
                    </View>
                </Modal>
            </View>
        )
    }
}
export default FilterModal

const styles = StyleSheet.create ({
    container: {
        alignItems: 'center',
        // backgroundColor: 'white',
        padding: 10,
        position: 'absolute',
        bottom: -200
    },
    modal: {
        position: 'relative',
        zIndex: 0,
        flexGrow: 1,
        justifyContent: 'center',
        // flex: 1,
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,.6)',
        paddingRight: 20,
        paddingLeft: 20
    },
    text: {
        color: '#3f2949',
        marginTop: 10
    },
    box: {
        width: '100%',
        backgroundColor: 'rgb(218, 218, 218)',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        borderRadius: 15,
        paddingBottom: 20
    },
    input: {
        width: '100%',
        fontSize: 16,
        paddingTop: 0,
        textAlign: 'right',
    },
    searchButton: {
        width: '90%',
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgb(20, 122, 170)'
    },
    searchText:{
        color: 'white'
    },
    picker: {
        height:50,
        width: "100%",
        alignSelf: 'flex-end'
    },
    label: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'rgb(60, 60, 60)'
    },
    headerContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 8,
        borderBottomWidth: 1,
        borderBottomColor: 'rgb(237, 237, 237)',
        marginBottom: 20
    },
    headerText: {
        fontSize: 14,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black'
    },
    advertise: {
        width: '35%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30,
        backgroundColor: '#8dc63f',
        padding: 5,
        marginTop: 10
    },
    buttonTitle: {
        fontSize: 14,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    inputLabel: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        alignSelf: 'flex-end',
        paddingRight: '10%',
        paddingBottom: 5
    },
    insContainer: {
        width: '100%',
        flexDirection: 'row',
        flexWrap: 'wrap',
        // alignItems: 'center',
        // justifyContent: 'space-between',
        padding: 8
    },
    info: {
        width: '100%',
        // alignItems: 'center',
        // justifyContent: 'flex-end',
        paddingTop: 20,
        paddingBottom: 20,
        borderBottomColor: 'white',
        borderBottomWidth: 1
    },
    rowsContainer: {
        width: '70%'
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    itemContainer: {
        width: '48%'
    },
    title: {
        color: 'rgb(60, 60, 60)',
        fontSize: 11,
        fontFamily: 'IRANSansMobile(FaNum)',
        alignSelf: 'flex-end',
        paddingRight: 15,
        paddingBottom: 10,
    },
    imageContainer: {
        borderRadius: 15,
        padding: 7,
        backgroundColor: 'white',
        width: '20%',
        marginRight: 10,
        marginLeft: 10
    },
})