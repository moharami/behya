import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(251, 251, 251)',
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'white',
        height: 165,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 99
    },
    top: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'white',
        height: 60,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 99,
        borderBottomColor: 'lightgray',
        borderBottomWidth: 1,
    },
    navbar: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        position: 'absolute',
        top: 70,
        right: 0,
        left: 0,
        zIndex: 999,
        paddingRight: 10,
        paddingLeft: 10,
    },
    body: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'flex-start',
        // paddingLeft: 15,
        // paddingRight: 15,
        // marginBottom: 20
    },
    bodyContainer: {
        paddingBottom: 270,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: '3%',
        marginRight: '3%'
    },
    filtering: {
        position: 'absolute',
        bottom: 25,
        width: '60%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 10,
        paddingLeft: 25,
        backgroundColor: 'rgb(231, 21, 56)',
        borderRadius: 30,

    },
    scroll: {
        paddingTop: 160
    },
    filterContainer: {
        // flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonTitle: {
        fontSize: 15,
        color: 'lightgray'
    },
    advLabel: {
        fontSize: 13,
        color: 'rgb(70, 70, 70)',
        paddingLeft: '23%',
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingTop: 35,
        alignSelf: 'center'
    }
});
