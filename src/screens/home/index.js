import React, {Component} from 'react';
import {ScrollView, View, AsyncStorage, TextInput, Alert, BackHandler, Text, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import Loader from '../../components/loader'
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import FIcon from 'react-native-vector-icons/dist/Feather';
import NavbarItem from '../../components/navbarItem'
import Advertise from "../../components/advertise/index";
import AppSearchBar from '../../components/searchBar'
import {store} from '../../config/store';
import Axios from 'axios'
import FiltetModal from './filterModal'
import url from '../../config/address'
Axios.defaults.baseURL = url;

import {
    SkypeIndicator,
} from 'react-native-indicators';

class Home extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: true,
            loadingMore: false,
            home: true,
            catPage: 2,
            catId: null,
            filterModal: false,
            catInfo: {},
            text: '',
            searchedItems: [],
            search: false
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);


        if(this.props.loged) {
            Axios.post('/index', {
            }).then(response=> {
                store.dispatch({type: 'USER_POSTS_FETCHED', payload: response.data});
                Axios.get('/categories', {
                }).then(response=> {
                    store.dispatch({type: 'USER_CATS_FETCHED', payload: response.data});
                    this.setState({loading: false });
                })
                    .catch((error) => {
                        Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                        this.setState({loading: false});
                    });
            })
                .catch((error) => {
                    Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                    this.setState({loading: false});
                });
        }
        else {
            Axios.post('/index', {
            }).then(response=> {
                store.dispatch({type: 'USER_POSTS_FETCHED', payload: response.data});
                Axios.get('/categories', {
                }).then(response=> {
                    store.dispatch({type: 'USER_CATS_FETCHED', payload: response.data});
                    this.setState({loading: false });
                })
                    .catch((error) => {
                        Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                        this.setState({loading: false});
                    });
            })
                .catch((error) => {
                    Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                    this.setState({loading: false});
                });
        }
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    filter(id) {
        this.setState({loading: true, home: false, search: false, catId: id});
        Axios.get('/category/'+id , {
        }).then(response=> {
            store.dispatch({type: 'ADV_CATS_FETCHED', payload: response.data.products.data});
            this.setState({loading: false, catInfo: response.data});
        })
        .catch((error) => {
            Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
            this.setState({loading: false});
        });
    }
    handleLoadMore() {
        console.log('cat id', this.state.catId)
        this.setState({loadingMore: true });
        Axios.get('/category/'+this.state.catId+'?page='+this.state.catPage
        ).then(response=> {
            if( response.data.products.data.length !== 0) {
                store.dispatch({type: 'NEW_CATEGORIES_FETCHED', payload: response.data.products.data});
                this.setState({loadingMore: false, catPage: ++this.state.catPage });
            }
            else {
                Alert.alert('', 'موارد بیشتری یافت نشد')
                this.setState({ loadingMore: false});
            }
        })
        .catch((error) => {
            this.setState({ loadingMore: false});
        });
    }
    closeModal() {
        this.setState({filterModal: false});
    }

    setModalVisible() {
        this.setState({filterModal: !this.state.filterModal});
    }
    startSearch(text){
        if(this.state.text === '') {
            Alert.alert('', 'عبارت مورد جستجو نمی تواند خالی باشد')
        }
        else {
            this.setState({ loading: true});
            Axios.get('/search?title='+this.state.text
            ).then(response=> {
                this.setState({loading: false, home: false, search: true, searchedItems: response.data.products});
            })
            .catch((error) => {
                Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                this.setState({loadingMore: false});
            });
        }
    }
    render() {
        const {posts, cats, advCats} = this.props;
        console.log('this.state.catInfo',this.state.catInfo)
        if(this.state.loading)
            return (<Loader/>)
        else return (
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={styles.top}>
                    <Icon name="bell" size={20} color="black" />
                    {/*<TouchableOpacity onPress={() => this.onBackPress()}>*/}
                        {/*<FIcon name="chevron-left" size={25} color="gray"  />*/}
                    {/*</TouchableOpacity>*/}
                    {/*<AppSearchBar placeholder="جستجو ..." />*/}
                    <View style={{width: '80%',  position: 'relative', zIndex: 1}}>
                        <TextInput
                            maxLength={15}
                            placeholder={'جستجو ...'}
                            placeholderTextColor={'gray'}
                            underlineColorAndroid='transparent'
                            value={this.state.text}
                            style={{
                                height: 35,
                                width: '90%',
                                color: 'gray',
                                fontSize: 14,
                                textAlign: 'right',
                                borderRadius: 20,
                                backgroundColor: 'rgb(239, 239, 239)',
                                paddingRight: 10
                            }}
                            onChangeText={(text) => this.setState({text: text})}
                        />
                        <TouchableOpacity onPress={() => this.startSearch(this.state.text)} style={{position: 'absolute', zIndex: 9992, top: '13%', left: '3%'}}>
                            <FIcon name="search" size={20} color="gray"  />
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity onPress={() => Actions.drawerOpen()}>
                        <Icon name="align-right" size={20} color="black" />
                    </TouchableOpacity>
                </View>
                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{paddingTop: 42, marginLeft: 10, marginRight: 10}}>
                    <TouchableOpacity onPress={() => this.setState({home: true, search: false})}>
                        <NavbarItem src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRaxtIpo0JNlf8zJ03gS6rDyylNVmcVi0mWkaSbol9Dyz9V4ntW" id={3} label='همه آگهی ها' all/>
                    </TouchableOpacity>
                    {
                        cats.map((item, index) =>
                        <TouchableOpacity onPress={() => this.filter(item.id)} key={index}>
                            <NavbarItem  src={item.app_image[0].file} id={index} label={item.title} />
                        </TouchableOpacity>
                        )
                    }
                </ScrollView>
            </View>
            <ScrollView style={styles.scroll}>
                <View style={styles.bodyContainer}>
                    <View style={[styles.body]}>
                        {
                            this.state.search ?
                                (
                                this.state.searchedItems.length !== 0 ? this.state.searchedItems.map((item, index) => <Advertise key={index} title={item.title} item={item} />) :
                                    <Text style={[styles.advLabel, {paddingLeft: '35%'}]}>موردی یافت نشد</Text>
                                )
                                :
                                this.state.home ? (
                                posts.length !== 0 ?
                                posts.map((item, index) => <Advertise key={index} title={item.title} item={item} />) : <Text style={styles.advLabel}>آگهی برای نمایش وجود ندارد</Text>)
                                :
                                (
                                    advCats.length !== 0 ?
                                        advCats.map((item, index) => <Advertise key={index} title={item.title} item={item} />) : <Text style={styles.advLabel}>آگهی برای نمایش وجود ندارد</Text>
                                )
                        }
                        {
                            !this.state.home && advCats.length !== 0 && !this.state.search?
                                <View style={{flexDirection: 'row', width: '100%', alignItems: 'center', justifyContent: 'center', paddingTop: 10}}>
                                    <TouchableOpacity onPress={() => this.handleLoadMore()}>
                                        <Text style={{textAlign: 'center', color: 'red'}}>مشاهده بیشتر</Text>
                                    </TouchableOpacity>
                                    {
                                        this.state.loadingMore ?
                                            <View style={{width: 40, height: 40}}>
                                                <SkypeIndicator color='rgb(231, 20, 56)' size={30} style={{height: 90}} />
                                            </View>
                                            : null
                                    }
                                </View> : null
                        }
                    </View>
                    <FiltetModal
                        closeModal={() => this.closeModal()}
                        modalVisible={this.state.filterModal}
                        onChange={(visible) => this.setModalVisible(visible)}
                        catInfo={this.state.catInfo}
                    />
                </View>
            </ScrollView>
            {
                !this.state.home && !this.state.search ?
                    <View style={styles.filterContainer}>
                        <TouchableOpacity onPress={() => this.setState({filterModal: true})} style={styles.filtering}>
                            <Icon name="ellipsis-h" size={24} color="lightgray" />
                            <Text style={styles.buttonTitle}>فیلتر کن !</Text>
                        </TouchableOpacity>
                    </View>
                    : null
            }
        </View>
        );
    }
}
function mapStateToProps(state) {
    return {
        posts: state.posts.posts,
        cats: state.posts.cats,
        advCats:  state.posts.advCats
    }
}
export default connect(mapStateToProps)(Home);
