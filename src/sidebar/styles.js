import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        // width: '100%',
        flex: 1,
        // flexDirection: 'row',
        position: 'relative',
        zIndex: 20,
        alignItems: 'flex-end',
        justifyContent: 'space-between',
        backgroundColor: 'rgb(116, 23, 154)',

    },
    bodyContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-between'
    },
    image: {
        width: 70,
        height: 70,
        borderRadius: 70,
        position: 'absolute',
        top: 40,
        right: 20

    },
    header: {
        alignItems: 'flex-end',
        justifyContent: 'center',
        padding: 50,
        paddingTop: 130
    },
    headerTitle: {
        color: 'white',
        fontSize: 27
    },
    headerBody: {
        color: 'white',
        opacity: .7,
        fontSize: 18
    },
    left: {
        padding: 30
    },
    bodyTitle: {
        color: 'white',
        fontSize: 18,
        paddingBottom: 20,
        opacity: .85
    },
    body: {
        paddingRight: 50,
        // paddingTop: 50
    },
    footer: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        opacity: .80,
        width: '100%',
        // padding: 15
    },
    footerTitle: {
        color: 'rgb(116, 23, 154)',
        fontSize: 23,
        // paddingBottom: 60
        padding: 20
    }
});
