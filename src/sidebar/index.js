
import React, {Component} from 'react';
import {Text, View,Image, AsyncStorage, Alert, TouchableOpacity} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {Actions} from 'react-native-router-flux'
import {connect} from 'react-redux';
import {store} from '../config/store';


class Sidebar extends React.Component {
    constructor(){
        super();
        this.state = {
            login: false
        };
    }
    componentWillMount() {
        AsyncStorage.getItem('token').then((info) => {
            if(info !== null) {
                // this.setState({login: false});
                store.dispatch({type: 'USER_LOGED', payload: true});
            }
            else {
                store.dispatch({type: 'USER_LOGED', payload: false});
            }
        });
    }
    logout() {
        AsyncStorage.removeItem('token');
        Alert.alert('','شما با موفقیت خارج شدید');
        store.dispatch({type: 'USER_LOGED', payload: false});
        Actions.drawerClose()
        Actions.login()
    }
    render(){
        const {loged} = this.props;
        return (
            <View style={styles.container}>
                <Image style={styles.image} source={{uri: 'http://ec.nahad.ir//uploads/default-user.png'}} />

                <View style={styles.bodyContainer}>
                    <View style={styles.left}>
                        <TouchableOpacity onPress={() => Actions.drawerClose()}>
                            <Icon name="close" size={20} color="white" />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.right}>
                        {
                            loged ?
                                <View style={styles.header}>
                                    {/*<Text style={styles.headerTitle}>وارد شوید</Text>*/}
                                    {/*<Text style={styles.headerBody}>شما وارد حساب کاربری نشده اید</Text>*/}
                                </View>
                                :
                                <View style={styles.header}>
                                    <TouchableOpacity onPress={()=> Actions.login()}>
                                        <Text style={styles.headerTitle}>وارد شوید</Text>
                                    </TouchableOpacity>
                                    <Text style={styles.headerBody}>شما وارد حساب کاربری نشده اید</Text>
                                </View>
                        }
                            {
                                loged ?
                                    <View style={styles.body}>
                                        <TouchableOpacity onPress={() => Actions.userAdvertise()}>
                                            <Text style={styles.bodyTitle}>آگهی های من</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => Actions.push('vitrin')}>
                                            <Text style={styles.bodyTitle}>ویترین ها</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => Actions.composeAdvertise()}>
                                            <Text style={styles.bodyTitle}>افزودن آگهی</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => Actions.addVitrin()}>
                                            <Text style={styles.bodyTitle}>افزودن ویترین</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => Actions.vitrin({loged: true})}>
                                            <Text style={styles.bodyTitle}>ویترین های من</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => Actions.chatUser()}>
                                            <Text style={styles.bodyTitle}>پیام خصوصی</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.logout()}>
                                            <Text style={styles.bodyTitle}>خروج</Text>
                                        </TouchableOpacity>
                                    </View>
                                    :
                                    <View style={styles.body}>
                                        <TouchableOpacity onPress={() => Actions.home()}>
                                            <Text style={styles.bodyTitle}>جدیدترین آگهی ها</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => Actions.push('vitrin')}>
                                            <Text style={styles.bodyTitle}>ویترین ها</Text>
                                        </TouchableOpacity>
                                    </View>
                            }

                    </View>
                </View>
                <TouchableOpacity onPress={() => Actions.login()} style={styles.footer}>
                    <Text style={styles.footerTitle}>عضویت</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
function mapStateToProps(state) {
    return {
        loged: state.auth.loged,
    }
}
export default connect(mapStateToProps)(Sidebar);

