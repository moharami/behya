import React, {Component} from 'react';
import {Dimensions} from 'react-native';
import {Router, Stack, Scene, Drawer, Actions} from 'react-native-router-flux'
import Sidebar from '../sidebar'
import Login from '../screens/login'
import Home from '../screens/home'
import FilterPage from '../screens/filterPage'
import ComposeAdvertise from '../screens/composeAdvertise'
import AdvertiseDetail from '../screens/advertiseDetail'
import Vitrin from '../screens/vitrin'
import AddVitrin from '../screens/addVitrin'
import VitrinDetail from '../screens/vitrinDetail'
import UserAdvertise from '../screens/userAdvertise'
import Chat from '../screens/chat'
import ChatUser from '../screens/chatUser'
import {store} from '../config/store';
import {Provider, connect} from 'react-redux';


class MainDrawerNavigator extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        return (
            <Provider store={store}>
                <Router sceneStyle={{backgroundColor: '#fff'}}>
                    <Drawer
                        drawerPosition='right'
                        key="drawer"
                        contentComponent={Sidebar}
                        drawerWidth={Dimensions.get('window').width}
                        hideNavBar>
                        <Scene store={store}>
                            <Scene key="login" initial={true} component={Login} title="Login" hideNavBar/>
                            <Scene key="home" component={Home} title="Home" hideNavBar/>
                            <Scene key="advertiseDetail" component={AdvertiseDetail} title="AdvertiseDetail" hideNavBar/>
                            <Scene key="filterPage" component={FilterPage} title="FilterPage" hideNavBar/>
                            <Scene key="vitrin" component={Vitrin} title="Vitrin" hideNavBar/>
                            <Scene key="composeAdvertise" component={ComposeAdvertise} title="ComposeAdvertise" hideNavBar/>
                            <Scene key="addVitrin" component={AddVitrin} title="AddVitrin" hideNavBar/>
                            <Scene key="vitrinDetail" component={VitrinDetail} title="VitrinDetail" hideNavBar/>
                            <Scene key="userAdvertise" component={UserAdvertise} title="UserAdvertise" hideNavBar/>
                            <Scene key="chat" component={Chat} title="Chat" hideNavBar/>
                            <Scene key="chatUser" component={ChatUser} title="ChatUser" hideNavBar/>
                        </Scene>
                    </Drawer>
                </Router>
            </Provider>
        );
    }
}
export default MainDrawerNavigator;



