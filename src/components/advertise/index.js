import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import moment from 'moment'
import 'moment/locale/fa';

class Advertise extends Component {
    constructor(props){
        super(props);
    }
    render() {
        return (
            <TouchableOpacity style={styles.container} onPress={() => Actions.advertiseDetail({advId: this.props.item.id}) }  >
                {
                    this.props.item.attachment.length !== 0 ?
                        <Image style={styles.image} source={{uri: "https://behya.com/uploads/products/"+this.props.item.attachment[0].file }} />
                        :
                        <Image style={styles.image} source={{uri: "http://behya.com/uploads/defaults/thumb_600-600_default.png" }} />
                }
                <View style={styles.content}>
                    <Text style={styles.title}>{this.props.title}</Text>
                    <View style={styles.advRow}>
                        <Text style={styles.bodyText}>{moment(this.props.item.created_at, "YYYYMMDD").fromNow()}</Text>
                        <Text style={styles.bodyText}>انتشار: </Text>
                    </View>
                    <View style={styles.advRow}>
                        <Text style={styles.bodyText}>{this.props.item.city.title !== null && this.props.item.city.title}</Text>
                        <Text style={styles.bodyText}>شهر:</Text>
                    </View>
                    <View style={styles.advRow}>
                        <Text style={styles.bodyText}>{this.props.item.list_price === -22 ? 'تماس بگیرید' : (this.props.item.list_price === -21 ? 'توافقی' : this.props.item.list_price+ ' ریال')}</Text>
                        <View style={styles.cameraContainer}>
                            <Text style={styles.bodyText}>{this.props.item.hit}</Text>
                            <Icon name="camera" size={20} color="black" />
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}
export default Advertise;