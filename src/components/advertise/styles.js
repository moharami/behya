import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        // flex: .48,
        width: '46%',
        borderRadius: 5,
        backgroundColor: 'white',
        position: 'relative',
        height: 260,
        elevation: 4,
        // margin: 5,
        marginRight: 7,
        marginBottom: 7,
        marginTop: 7,

    },
    image: {
        width: '100%',
        resizeMode: 'contain',
        height: 240,
        borderTopRightRadius: 5,
        borderTopLeftRadius: 5,
        position: 'absolute',
        top: -60,
        left: 0,
        right: 0,
        bottom: 0,
    },
    title: {
        fontSize: 13,
        color: 'rgb(70, 70, 70)',
        paddingBottom: 10,
        textAlign: 'right',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    advRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 5
    },
    cameraContainer: {
        flexDirection: 'row',
    },
    content: {
        position: 'absolute',
        top: 125,
        left: 0,
        right: 0,
        bottom: 0,
        padding: 5,
        backgroundColor: 'white'
    },
    bodyText: {
        paddingRight: 4,
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)',


    }
});