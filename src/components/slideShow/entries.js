export const ENTRIES1 = [
    {

        illustration: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcReU3nuIlN2ak9CJfglwqWe5F_fwHTBD7U4vARGkK36dZaF3yZp'
    },
    {

        illustration: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcReU3nuIlN2ak9CJfglwqWe5F_fwHTBD7U4vARGkK36dZaF3yZp'
    },
    {

        illustration: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcReU3nuIlN2ak9CJfglwqWe5F_fwHTBD7U4vARGkK36dZaF3yZp'
    },
    {

        illustration: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcReU3nuIlN2ak9CJfglwqWe5F_fwHTBD7U4vARGkK36dZaF3yZp'
    },
    {

        illustration: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcReU3nuIlN2ak9CJfglwqWe5F_fwHTBD7U4vARGkK36dZaF3yZp'
    },

];

export const ENTRIES2 = [
    {
        title: 'Favourites landscapes 1',
        subtitle: 'Lorem ipsum dolor sit amet',
        illustration: 'https://i.imgur.com/SsJmZ9jl.jpg'
    },
    {
        title: 'Favourites landscapes 2',
        subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
        illustration: 'https://i.imgur.com/5tj6S7Ol.jpg'
    },
    {
        title: 'Favourites landscapes 3',
        subtitle: 'Lorem ipsum dolor sit amet et nuncat',
        illustration: 'https://i.imgur.com/pmSqIFZl.jpg'
    },
    {
        title: 'Favourites landscapes 4',
        subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
        illustration: 'https://i.imgur.com/cA8zoGel.jpg'
    },
    {
        title: 'Favourites landscapes 5',
        subtitle: 'Lorem ipsum dolor sit amet',
        illustration: 'https://i.imgur.com/pewusMzl.jpg'
    },
    {
        title: 'Favourites landscapes 6',
        subtitle: 'Lorem ipsum dolor sit amet et nuncat',
        illustration: 'https://i.imgur.com/l49aYS3l.jpg'
    }
];