import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {Actions} from 'react-native-router-flux'


class NavbarItem extends Component {
    constructor(props){
        super(props);
    }
    filter() {
        Actions.push('filterPage')
    }
    getRandomColor(id){
        let colorValues = ["rgb(241, 220, 75)", "rgb(234, 136, 123)", "rgb(115, 131, 146)", "rgb(86, 210, 199)", "rgb(92, 207, 140)", "rgb(241, 220, 75)", "rgb(234, 136, 123)", "rgb(115, 131, 146)", "rgb(86, 210, 199)", "rgb(92, 207, 140)"];
        // return colorValues[Math.floor(Math.random() * colorValues.length)];
        return colorValues[id];
    }
    render() {
        const imgSrc = 'https://behya.com/uploads/productcats/'+this.props.src
        return (
        <View style={{
            width: this.props.small? 35 :80,
            alignItems: 'center',
            justifyContent: 'flex-start',
            // paddingLeft: 20,
            marginTop: this.props.small ? 10 : 0
        }}>
            <View style={{
                padding: this.props.small? 0: 15,
                width: this.props.small? 35: 50,
                height: this.props.small? 35: 50,
                borderRadius: 60,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: this.getRandomColor(this.props.id)
            }}
            >
                {
                    this.props.all ?
                        <Image source={{uri: "https://www.evisa.gov.kh/images/icon/home.png"}} style={{width: 28, height: 28}} />
                    :
                        (this.props.icon ?
                                <Icon name="truck" color="white" size={20} />
                            :
                                <Image source={{uri: imgSrc}} style={{width: 28, height: 28}} />
                        )
                }
            </View>
            <Text style={{paddingTop: 5,
                fontSize: 12,
                color: 'gray',
                fontFamily: 'IRANSansMobile(FaNum)'
            }}>{this.props.label}</Text>
        </View>

        );
    }
}
export default NavbarItem;