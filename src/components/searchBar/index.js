import React, {Component} from 'react';
import {View} from 'react-native';
import {List, SearchBar} from "react-native-elements";

class AppSearchBar extends Component {
    constructor(props){
        super(props);
    }
    _doSearch(){

    }
    render() {
        return (
            <View style={{width: '75%'}}>
                <SearchBar
                    containerStyle={{
                        backgroundColor: 'transparent',
                        borderWidth: 0,
                        shadowColor: 'white',
                        borderBottomColor: 'transparent',
                        borderTopColor: 'transparent'}}
                    onChangeText={(text) => this._doSearch(text)}
                    onClearText={() => this.setState({value: '', newses: []})}
                    placeholder={this.props.placeholder}
                    round
                    inputStyle={{textAlign: 'right', backgroundColor: 'rgb(249, 249, 249)'}}
                />
            </View>
        );
    }
}
export default AppSearchBar;