import React, {Component} from 'react';
import {View, Text, TouchableOpacity, ImageBackground} from 'react-native';
import styles from './styles'
class VerticalImage extends Component {
    constructor(props){
        super(props);
    }
    render() {
        return (
            <View style={styles.imagesContainer}>
                <ImageBackground source={{uri: 'https://img.gaadicdn.com/images/mycar/large/maruti/alto-k10/marketimg/alto-k10.webp'}}
                                 style={styles.subImage}>
                    <View style={{backgroundColor:'rgba(0,0,0,.6)',
                        height: 80,
                        width: "100%",
                        paddingRight:10,
                        paddingBottom: 30,
                        alignItems: "flex-end",
                        justifyContent: "flex-end",
                    }}>
                        <Text style={styles.imageText}>دنیای موتور سیکلت</Text>
                    </View>
                </ImageBackground>
                <ImageBackground source={{uri: 'https://img.gaadicdn.com/images/mycar/large/maruti/alto-k10/marketimg/alto-k10.webp'}}
                                 style={styles.subImage}>
                    <View style={{backgroundColor:'rgba(0,0,0,.6)',
                        height: 80,
                        width: "100%",
                        paddingRight:10,
                        paddingBottom: 30,
                        alignItems: "flex-end",
                        justifyContent: "flex-end",
                    }}>
                        <Text style={styles.imageText}>دنیای موتور سیکلت</Text>
                    </View>
                </ImageBackground>
            </View>


        );
    }
}
export default VerticalImage;