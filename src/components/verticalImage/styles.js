import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    imagesContainer: {
        flex: 1,
        flexDirection: 'column',
        width: "60%",
        alignItems: 'center',
        justifyContent: 'space-between',
        height: 170
    },
    subImage: {
        width: 200,
        height: 30,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
    },
    imageText: {
        backgroundColor: "transparent",
        fontSize: 14,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: "white"

    },
});