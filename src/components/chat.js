import React, {Component} from 'react';
import { Actions } from 'react-native-router-flux';
import {Image, ImageBackground, TouchableOpacity} from "react-native";
import {Icon, Text, View} from "native-base";
import {url} from "./helper";
import {chat, shopL} from "../assets/styles";
export default class ChatComponent extends Component{
    constructor(props){
        super(props)
        this.state={
        }
    }
    render(){
        const { ch ,user } = this.props;
        return(
            ch.from == user?
                <View style={[chat.myChatParent,{ transform: [{ scaleY: -1 }]}]}>
                    <View style={chat.myChat}>
                        <Text style={chat.myChatText}>{ch.text}</Text>
                    </View>
                </View>:
                <View style={[chat.otherChatParent,{ transform: [{ scaleY: -1 }]}]}>
                    <View style={chat.otherChat}>
                        <Text style={chat.otherChatText}>{ch.text}</Text>
                    </View>
                </View>
        )
    }
}