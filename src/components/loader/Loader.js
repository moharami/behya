import React from 'react';
import {View, Text, ActivityIndicator} from 'react-native';
import {
    SkypeIndicator,
} from 'react-native-indicators';

import styles from './styles';

const Loader = (props) => {
    return (
        <View style={styles.container}>
            {/*<Text style={[styles.text, {color: 'red'}]}>در حال دریافت اطلاعات</Text>*/}
            <View style={{width: '100%', height: 60}}>
                <SkypeIndicator color='rgb(231, 20, 56)' size={50} style={{height: 90}} />
            </View>
            <Text style={[styles.text, {color: 'rgb(50, 50, 50)', paddingTop: 20}]}>در حال دریافت اطلاعات</Text>
        </View>
    );
};
export default Loader;

