import React, {Component} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

class DetailNavbar extends Component {
    constructor(props){
        super(props);
    }
    render() {
        return (
            <TouchableOpacity>
                <View style={{backgroundColor: this.props.bgColor,
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'flex-start',
                    paddingTop: 8,
                    paddingBottom: 8,
                    paddingLeft: 10,
                    paddingRight: 25,
                    borderRadius: 20,
                    marginLeft: 5,
                    marginRight: this.props.space ? 10: this.props.latest ? 30 : 0
                }}>
                    <Icon name={this.props.icon} size={16} color="white" style={{paddingRight: 15}} />
                    <Text style={{color: 'white'}}>{this.props.label}</Text>
                </View>
            </TouchableOpacity>


        );
    }
}
export default DetailNavbar;