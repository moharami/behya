
import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {Actions} from 'react-native-router-flux'
import moment from 'moment'
import 'moment/locale/fa';
import Selectbox from 'react-native-selectbox'

class SelectFilter extends Component {
    constructor(props){
        super(props);
        this.state = {
            value: {key: 0, label: this.props.label, value: 0}
        }
    }
    render() {
        return (
            <View style={{position: 'relative', elevation: this.props.compose ? 0 : 5, zIndex: 3, width: this.props.compose ? '100%' : '90%', backgroundColor: 'white', height: 36, borderRadius: this.props.compose ? 15 : 10,  borderColor: this.props.compose ? 'lightgray' :  'gray', borderWidth: 1, marginBottom: 10}}>
                {/*<FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>*/}
                <Selectbox
                    style={{height: 42, paddingTop: '3%', paddingRight: 10}}
                    selectLabelStyle={{textAlign: 'right', color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingTop: 0}}
                    optionLabelStyle={{textAlign: 'center', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                    selectedItem={this.state.value}
                    cancelLabel="لغو"
                    onChange={(itemValue) =>{
                        this.setState({
                            value: itemValue
                        });
                        this.props.changeFilter(itemValue.value, this.props.fId)
                    }}
                    items={this.props.items} />
            </View>

        );
    }
}
export default SelectFilter;