import React, {Component} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

class FeatureItem extends Component {
    constructor(props){
        super(props);
    }
    render() {
        return (
            <TouchableOpacity>
                <View style={{
                    backgroundColor: this.props.bgColor,
                    alignItems: 'center',
                    justifyContent: 'center',
                    paddingTop:  this.props.price? 2: 4,
                    paddingBottom: this.props.price? 2: 4,
                    paddingLeft: this.props.price ? 7: 23,
                    paddingRight: this.props.price ? 7: 23,
                    borderRadius: 20,
                    marginLeft: 5,
                    borderColor: this.props.price? 'rgb(82, 45, 99)': 'rgb(73, 221, 185)',
                    borderWidth: this.props.price ? 3: this.props.filter? 0 : 1
                }}>
                    <Text style={{color: this.props.price? 'rgb(82, 45, 99)': 'gray', fontSize: this.props.price? 13 : 15,fontFamily: this.props.price? 'IRANSansMobile(FaNum)_Black' : 'IRANSansMobile(FaNum'}}>{this.props.label}</Text>
                </View>
            </TouchableOpacity>


        );
    }
}
export default FeatureItem;