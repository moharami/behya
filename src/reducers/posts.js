
const INITIAL_STATE = {
    posts: [],
    cats: [],
    advCats: [],
    vitrins: [],
    search: false
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'USER_POSTS_FETCHED':
            return { ...state, posts: action.payload };
        case 'USER_CATS_FETCHED':
            return { ...state, cats: action.payload };
        case 'ADV_CATS_FETCHED':
            return { ...state, advCats: action.payload };
        case 'SEARCH_PAGE_ENABLED':
            return { ...state, search: true };
            case 'SEARCH_PAGE_REMOVED':
            return { ...state, search: false };
        case 'NEW_CATEGORIES_FETCHED':
            const newArray2 = [...state.advCats, ...action.payload];
            console.log('new payload', newArray2)
            return {
                ...state,
                advCats: newArray2,
            };
        case 'VITRINS_FETCHED':
            return { ...state, vitrins: action.payload };
        default:
            return state;
    }
};
