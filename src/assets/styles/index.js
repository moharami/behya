import EStyleSheet from 'react-native-extended-stylesheet';

export const login = EStyleSheet.create({
    container: {backgroundColor: '#f4f4f4', flex: 1,paddingRight:20,paddingLeft:20},
    input:{backgroundColor:'white',borderColor:'white',marginTop:50,textAlign:'right',padding:5,paddingLeft:8,marginLeft:0,borderRadius:0},
    inputCode:{backgroundColor:'white',borderColor:'white',marginTop:15,textAlign:'right',padding:5,marginLeft:0,borderRadius:0},
    inputChild:{fontFamily:'$font',textAlign:'right'},
    inputCodeChild:{fontFamily:'$font',textAlign:'center'},
    inputButton:{marginBottom:0,padding:0,top:3,bottom:0,backgroundColor:'$mainColor'},
    inputButtonText:{color:'white',fontFamily:'$font',fontSize:18},
    loginButton:{marginTop:30,backgroundColor:'$mainColor',height:60,borderRadius:0},
    loginButtonText:{color:'#454545',textAlign:'right',fontSize:18,fontFamily:'$font'},
    logoParent:{display:'flex',alignItems:'center',marginTop:50}
})
export const signup = EStyleSheet.create({
    container: {backgroundColor: '#f4f4f4', flex: 1,paddingRight:20,paddingLeft:20,paddingTop:50},
    inputCode:{backgroundColor:'white',borderColor:'white',marginTop:15,textAlign:'right',padding:5,marginLeft:0,borderRadius:0},
    inputCodeChild:{fontFamily:'$font',textAlign:'right'},
    picker:{width:'100%',backgroundColor: '#fff',marginTop:15},
    pickerText:{fontFamily:'$font'},
    inputButton:{marginBottom:0,padding:0,top:3,bottom:0,backgroundColor:'$mainColor'},
    inputButtonText:{color:'white',fontFamily:'$font',fontSize:18},
    loginButton:{marginTop:30,backgroundColor:'$mainColor',height:60,borderRadius:0,marginBottom:20},
    loginButtonText:{color:'#454545',textAlign:'right',fontSize:18,fontFamily:'$font'},
    logoParent:{justifyContent:'center',display:'flex',alignItems:'center',marginTop:100},
    shopButton:{height:80,backgroundColor:'#707070',borderRadius:10},
    shopButtonIcon:{fontSize:35},
    shopButtonText1:{fontFamily:'$font',color:'white',fontSize:20,textAlign:'right'},
    shopButtonText2:{fontFamily:'$font',color:'white',textAlign:'right'},
    switchText:{fontFamily:'$font',textAlign:'right',marginRight:5,color:'#454545',},
    expertTitle:{fontFamily:'$font',fontSize:20,textAlign:'right',marginTop:20},
    catTitle:{fontFamily:'$font',fontSize:18,textAlign:'right'},
    subCatParent:{flexDirection:'row-reverse',flexWrap:'wrap',marginTop:15},
    textArea:{width:'100%',borderColor:'transparent',textAlign:'right',fontFamily:'$font'},
    imgTitle:{textAlign:'center',fontFamily:'$font',fontSize:16,marginTop:10},
    description:{textAlign:'center',fontFamily:'$font',lineHeight:35},
    shopDesc:{textAlign:'center',fontFamily:'$font',lineHeight:35},
    checkBoxText:{fontFamily:'$font',textAlign:'right'}
})
export const createAd = EStyleSheet.create({
    container: {backgroundColor: '#f4f4f4', flex: 1,paddingRight:20,paddingLeft:20,paddingTop:50},
    loaderParent:{backgroundColor:'white',justifyContent:'center',alignItems:'center',padding:10,marginTop:10},
    inputCode:{backgroundColor:'white',borderColor:'white',marginTop:15,textAlign:'right',padding:5,marginLeft:0,borderRadius:0},
    inputCodeChild:{fontFamily:'$font',textAlign:'right'},
    editedTextArea:{fontFamily:'$font',textAlign:'right',height:100},
    picker:{width:'100%',backgroundColor: '#fff',marginTop:15},
    pickerText:{fontFamily:'$font'},
    inputButton:{marginBottom:0,padding:0,top:3,bottom:0,backgroundColor:'$mainColor'},
    inputButtonText:{color:'white',fontFamily:'$font',fontSize:18},
    Button:{marginTop:30,backgroundColor:'$mainColor',height:60,borderRadius:0,marginBottom:15},
    ButtonText:{color:'#454545',textAlign:'right',fontSize:18,fontFamily:'$font'},
    imgParent:{flexDirection:'row-reverse',marginTop:5,flexWrap:'wrap',justifyContent:'space-between'},
    textArea:{width:'100%',borderColor:'transparent',textAlign:'right',fontFamily:'$font'},
    switchText:{fontFamily:'$font',textAlign:'right',marginRight:5,color:'#454545',},
    inputLabel:{fontFamily:'$font',textAlign:'right'},
    price:{fontFamily:'$font',textAlign:'right',marginTop:5},
})
export const list = EStyleSheet.create({
    bold:{
        '@media ios' : {
            fontWeight : "bold"
        },
        '@media android' : {
            fontFamily : 'IRANYekanMobileBold',
        },
    },
    loaderParent:{backgroundColor:'white',justifyContent:'center',alignItems:'center',padding:10,marginTop:10},
    container: {backgroundColor: '#f4f4f4', flex: 1},
    searchParent:{backgroundColor:'white',padding:15},
    searchItem:{borderRadius:5},
    searchInput:{fontFamily:'$font',textAlign:'right',marginLeft:0,paddingRight:100,borderColor:'#dcdcdc'},
    catParent:{paddingRight:10,paddingLeft:10,flexDirection:'row-reverse',flexWrap:'wrap'},
    catChild:{width:'25%',paddingLeft:2.5,paddingRight:2.5,marginTop:5},
    category:{height:70,backgroundColor:'white',borderRadius:5,textAlign:'center',alignItems:'center'},
    categorySelected:{height:70,backgroundColor:'$mainColor',borderRadius:5,textAlign:'center',alignItems:'center'},
    categoryImage:{marginTop:5,height:27,width:27},
    categoryImageSelected:{marginTop:5,height:27,width:27,tintColor:'white'},
    categoryText:{marginTop:5,fontFamily:'$font',color:'#454545',fontSize:13},
    listParent:{marginTop:5,flex:1,flexDirection:'row-reverse',backgroundColor:'white',marginRight:10,marginLeft:10},
    imageParent:{width:'40%'},
    importantElement:{fontFamily:'$font',fontSize:15,color:'#454545',textAlign:'right'},
    camIcon:{fontSize:20,marginRight:5,color:'$mainColor'},
    detailParent:{width:'60%',paddingRight:15,paddingLeft:15},
    detail:{flexDirection:'row-reverse',justifyContent:'space-between',marginTop:5},
    detailText:{fontFamily:'$font',color:'#aaa',fontSize:14},
    detailTitle:{fontFamily:'$font',fontSize:20,color:'#585858',textAlign:'right',marginTop:5},
    pageFooterButton:{backgroundColor:'$mainColor',borderRadius:0},
    pageFooterButtonMain:{backgroundColor:'#ffdb4b',borderRadius:0},
    footerIcon:{color:'#454545',marginLeft:0,marginRight:0},
    footerText:{color:'#454545',fontFamily:'$font',marginRight:10,fontSize:14,paddingRight:0,paddingLeft:0,marginTop:2},
    shopBackground:{width:'100%'},
    shopTitle:{fontFamily:'$font',color:'#454545',fontSize:18,textAlign:'right',paddingLeft:50},
    shopText:{fontFamily:'$font',color:'#454545',textAlign:'right',paddingLeft:50,lineHeight:30},
    shopButton:{backgroundColor:'$mainColor',height:35,borderRadius:7},
    shopButtonText:{color:'#454545',fontFamily:'$font'},
    star:{color:'$mainColor',fontSize:20},
    starText:{color:'#454545',marginLeft:5,fontSize:14,fontFamily:'$font'},
    suggestion:{padding:5,backgroundColor:'rgba(17,17,37,.8)',position:'absolute',bottom:0,left:0,right:0},
    suggestionText:{textAlign:'center',fontFamily:'$font',color:'white',fontSize:18},
    createButton:{backgroundColor:'$mainColor',height:60},
    createButtonText:{fontFamily:'$font',fontSize:20},
    badgeText:{fontFamily:'$font',color:'#f39c12'},
    shopStar:{position:'absolute',top:0,right:0,borderRadius:0},
    filterButton:{backgroundColor:'$mainColor',position:'absolute',right:0,flexDirection:'row-reverse',height:50,borderTopLeftRadius:0,borderBottomLeftRadius:0},
    filterButtonIcon:{marginLeft:5,padding:0,marginRight:5},
    filterButtonText:{fontFamily:'$font',paddingRight:0,paddingLeft:5},
    noAd:{fontFamily:'$font',textAlign:'center',marginTop:10,fontSize:13,marginRight:10,marginLeft:10},
    shopStarText:{fontFamily:'$font',fontSize:12},
    TenderCatParent:{flexDirection:'row-reverse',flexWrap:'wrap',justifyContent:'flex-start',padding:10},
    TenderCat:{paddingTop:5,paddingBottom:5,paddingLeft:10,paddingRight:10,borderRadius:2,backgroundColor:'white',marginRight:5,marginLeft:5},
    TenderCatText:{fontFamily:'$font'},
    specialText:{fontFamily:'$font'}
})
export const shopL = EStyleSheet.create({
    bold:{
        '@media ios' : {
            fontWeight : "bold"
        },
        '@media android' : {
            fontFamily : 'IRANYekanMobileBold',
        },
    },
    container: {backgroundColor: '#f4f4f4', flex: 1},
    searchParent:{backgroundColor:'white',padding:15},
    searchItem:{borderRadius:5},
    searchInput:{fontFamily:'$font',textAlign:'right',marginLeft:0,paddingRight:40,borderColor:'#dcdcdc'},
    pageFooterButton:{backgroundColor:'$mainColor',borderRadius:0},
    pageFooterButtonMain:{backgroundColor:'#ffdb4b',borderRadius:0},
    footerIcon:{color:'#454545',marginLeft:0,marginRight:0},
    footerText:{color:'#454545',fontFamily:'$font',marginRight:10,fontSize:14,paddingRight:0,paddingLeft:0,marginTop:2},
    shopBackground:{width:'100%'},
    shopTitle:{fontFamily:'$font',color:'#454545',fontSize:18,textAlign:'right',paddingLeft:50},
    shopText:{fontFamily:'$font',color:'#454545',textAlign:'right',lineHeight:30},
    shopButton:{backgroundColor:'$mainColor',height:35,borderRadius:7},
    shopButtonText:{color:'#454545',fontFamily:'$font'},
    star:{fontSize:20},
    starText:{color:'#454545',marginLeft:5,fontSize:14,fontFamily:'$font'},
})
export const single = EStyleSheet.create({
    bold:{
        '@media ios' : {
            fontWeight : "bold"
        },
        '@media android' : {
            fontFamily : 'IRANYekanMobileBold',
        },
    },
    container: {backgroundColor: '#f4f4f4', flex: 1},
    mainTitle:{
        '@media ios' : {
            fontWeight : "bold",
            fontFamily:'$font'
        },
        '@media android' : {
            fontFamily : 'IRANYekanMobileBold',
        },
        fontSize:20,color:'#585858',textAlign:'right',},
    title:{fontFamily:'$font',textAlign:'right',fontSize:19,color:'#585858',marginTop:20},
    whiteBox:{paddingRight:25,paddingLeft:25,paddingTop:10,paddingBottom:10,backgroundColor:'white',},
    date:{fontFamily:'$font',color:'#949494',fontSize:15,textAlign:'right',marginTop:10},
    detailView:{flexDirection:'row-reverse',justifyContent:'space-between',marginTop:10},
    detail:{fontFamily:'$font',color:'#585858'},
    detailDesc:{fontFamily:'$font',color:'#585858',textAlign:'right',lineHeight:30,marginTop:20},
    special:{position:'absolute',bottom:0,right:25,backgroundColor:'#de7426',paddingTop:3,paddingLeft:15,paddingRight:15,paddingBottom:3},
    specialText:{fontFamily:'$font',color:'white'},
    buttonParent:{flexDirection:'row',padding:10,backgroundColor:'white',marginTop:5,marginBottom:10},
    buttonParentEdit:{flexDirection:'row',padding:10,backgroundColor:'white'},
    chatButton:{backgroundColor:'$mainColor',borderRadius:0},
    reqButton:{backgroundColor:'#003471',borderRadius:0},
    deleteButton:{backgroundColor:'#e74c3c',borderRadius:0},
    editButton:{backgroundColor:'#f39c12',borderRadius:0},
    chatButtonText:{fontFamily:'$font',color:'white',fontSize:17},
    pageFooterButton:{backgroundColor:'$mainColor',borderRadius:0},
    pageFooterButtonMain:{backgroundColor:'#ffdb4b',borderRadius:0},
    footerIcon:{color:'#454545',marginLeft:0,marginRight:0},
    footerText:{color:'#454545',fontFamily:'$font',marginRight:10,fontSize:14,paddingRight:0,paddingLeft:0,marginTop:2},
    price:{fontFamily:'$font',fontSize:18,},
    offerInput:{flex:.5,fontFamily:'$font',backgroundColor:'#f4f4f4',textAlign:'center'},
    offerBtn:{backgroundColor:'$mainColor',borderRadius:0,elevation:0,height:50},
    offerBtnText:{color:'#454545',fontFamily:'$font',fontSize:16},
    offerPic:{width:50,height:50,borderRadius:25},
    offerUser:{fontFamily:'$font',fontSize:16,},
    expertHeader:{padding:10,backgroundColor:'$mainColor',flexDirection:'row',justifyContent:'center'},
    expertHeaderText:{fontFamily:'$font',fontSize:20,color:'#454545'},
    warningIcon:{marginLeft:10,color:'$mainColor',fontSize:55},
    warningText:{fontFamily:'$font',textAlign:'right',paddingRight:20,paddingLeft:20,color:'#939393'},
    relatedTitle:{fontFamily:'$font',textAlign:'right',marginBottom:10,marginRight:10},
    content:{fontFamily:'$font'}
})
export const expert = EStyleSheet.create({
    bold:{
        '@media ios' : {
            fontWeight : "bold"
        },
        '@media android' : {
            fontFamily : 'IRANYekanMobileBold',
        },
    },
    container: {backgroundColor: '#f4f4f4', flex: 1},
    whiteBox:{backgroundColor:'white',marginTop:20,padding:20},
    profilePicture:{width:150,height:150,borderRadius:75,borderWidth:5,borderColor:'$mainColor',overflow:'hidden'},
    userName:{marginTop:10,fontFamily:'$font',textAlign:'center',fontSize:22,color:'#454545',},
    details:{paddingRight:30,paddingLeft:30,marginTop:20},
    detailBox:{flexDirection:'row-reverse',justifyContent:'space-between',marginTop:10},
    detailBadge:{borderRadius:0,backgroundColor:'#197b30'},
    detailText:{fontFamily:'$font',color:'#454545'},
    badgeText:{fontFamily:'$font'},
    buttonParent:{flexDirection:'row-reverse',},
    reqButton:{borderRadius:0,backgroundColor:'#003471'},
    cmButton:{borderRadius:0,backgroundColor:'#555555'},
    buttonText:{color:'white',fontFamily:'$font',fontSize:16},
    title:{fontSize:18,fontFamily:'$font',color:'#454545',textAlign:'right'},
    desc:{marginTop:20,fontFamily:'$font',color:'#454545',textAlign:'right',lineHeight:30},
    tags:{fontFamily:'$font',margin:5},
    pageFooterButton:{backgroundColor:'$mainColor',borderRadius:0},
    pageFooterButtonMain:{backgroundColor:'#ffdb4b',borderRadius:0},
    footerIcon:{color:'#454545',marginLeft:0,marginRight:0},
    footerText:{color:'#454545',fontFamily:'$font',marginRight:10,fontSize:14,paddingRight:0,paddingLeft:0,marginTop:2},
    userComment:{flexDirection:'row-reverse',padding:5,borderBottomWidth:1,borderBottomColor:'#eee',alignItems:'center'},
    expertCommentImage:{width:60,height:60,borderRadius:50,marginLeft:10},
    expertCommentName:{fontFamily:'$font',fontSize:15,color:'#454545',marginRight:10},
    cmParent:{padding:5},
    cmText:{fontFamily:'$font',textAlign:'right'}
})
export const filter = EStyleSheet.create({
    lightBox:{flex:1,position:'absolute',top:0,right:0,left:0,bottom:0,backgroundColor:'rgba(0,0,0,.5)',zIndex:5000,alignItems:'center',justifyContent:'center'},
    lightBoxContent:{width:'90%',backgroundColor:'white',borderRadius:4,zIndex:200},
    priceInputParent:{width:'50%',paddingRight:5,paddingLeft:5},
    priceInput:{fontFamily:'$font',fontSize:14,backgroundColor:'#f4f4f4',borderWidth:0},
    picker:{width:'100%',backgroundColor: '#f4f4f4',marginTop:15},
    smallPicker:{width:'100%',backgroundColor: '#f4f4f4',borderWidth:0},
    pickerText:{fontFamily:'$font'},
    Button:{backgroundColor:'$mainColor',height:50,borderBottomLeftRadius:4,borderBottomRightRadius:4},
    ButtonText:{color:'#454545',fontFamily:'$font',},
    description:{fontFamily:'$font',marginTop:20,textAlign:'center'},
    expertParent:{flexDirection:'row-reverse',padding:5,height:110,justifyContent:'flex-start'},
    expertImage:{width:80,height:80,borderRadius:50,marginLeft:10},
    expertName:{fontFamily:'$font',fontSize:17,color:'#454545'},
    nothingText:{fontFamily:'$font',textAlign:'center',fontSize:18},
    title:{fontFamily:'$font',textAlign:'right'},
    exitText:{fontFamily:'$font',textAlign:'center'},
})
export const Fabrika = EStyleSheet.create({
    container: {backgroundColor: '#f4f4f4', flex: 1},
    buttons:{padding:10,flexDirection:'row-reverse',width:'100%'},
    text:{marginRight:15,fontFamily:'$font',fontSize:18},
    icons:{color:'$mainColor',fontSize:30,width:40,textAlign:'center'},
    expertParent:{flexDirection:'row-reverse',padding:10,justifyContent:'space-between'},
    expertTitle:{fontFamily:'$font',textAlign:'right',marginBottom:10,marginRight:10,marginTop:10},
    expertUserPix:{width:64,height:64,borderRadius:32},
    expertUserText:{fontFamily:'$font',textAlign:'right'},
    btnText:{fontFamily:'$font'},
    imageParent:{justifyContent:'center',alignItems:'center',padding:10},
    img:{width:150,height:150,borderWidth:3,borderColor:'$mainColor',borderRadius:75},
    userText:{fontFamily:'$font',fontSize:22,marginTop:10,textAlign:'center',color:'white'},
    commentButtonText:{fontFamily:'$font',color:'white'},
    commentButton:{backgroundColor:'$mainColor',borderRadius:0,marginTop:10}

})
export const splashSC = EStyleSheet.create({
    bold:{
        '@media ios' : {
            fontWeight : "bold"
        },
        '@media android' : {
            fontFamily : 'IRANYekanMobileBold',
        },
    },
    container: {backgroundColor: '$mainColor', flex: 1},
    containerSlider: {backgroundColor: '#f4f4f4', flex: 1},
    logoParent:{flex:.5,justifyContent:'flex-start',alignItems:'center'},
    mainTitle:{fontFamily:'$font',color:'white',fontSize:40,lineHeight:40,marginTop:10,textAlign:'center'},
    text:{fontFamily:'$font',color:'white',fontSize:18,marginTop:10,lineHeight:18,textAlign:'center'},
    copyrightParent:{justifyContent:'center',alignItems:'center',flex:.25},
    version:{fontSize:12,color:'#8a8a8a'},
    copyright:{fontFamily:'$font',textAlign:'center',marginTop:10,fontSize:15,color:'#565656'},
    slide:{flex: 1,},
    image:{flex:.5,width:'100%'},
    descParent:{flex:.5,justifyContent:'center',alignItems:'center',padding:20},
    descTitle:{fontFamily:'$font',fontSize:20,color:'#777777',textAlign:'center'},
    desc:{fontFamily:'$font',fontSize:15,color:'#777777',textAlign:'center',marginTop:10},
    buttonParent:{alignItems:'center',justifyContent:'center',padding:50,flex:.5},
    button:{backgroundColor:'$mainColor',borderRadius:0,},
    buttonText:{fontFamily:'$font',color:'#454545'},

})
export const chat = EStyleSheet.create({
    bold:{
        '@media ios' : {
            fontWeight : "bold"
        },
        '@media android' : {
            fontFamily : 'IRANYekanMobileBold',
        },
    },
    container: {flex: 1,backgroundColor:'#f4f4f4',},
    userChatParent:{paddingRight:20,paddingLeft:20},
    userButton:{paddingTop:10,paddingBottom:10,flexDirection:'row-reverse',justifyContent:'flex-start',alignItems:'center',width:'100%'},
    userImg:{width:64,height:64,borderRadius:32,marginLeft:20},
    userName:{fontFamily:'$font',fontSize:14,textAlign:'right', color: 'white'},
    userChat:{fontFamily:'$font',fontSize:12,textAlign:'right'},
    header:{backgroundColor:'$mainColor',flexDirection:'row-reverse',paddingRight:15,paddingLeft:15,paddingTop:5,paddingBottom:5,justifyContent:'space-between',alignItems:'center'},
    bottom:{flexDirection:'row-reverse',paddingRight:15,paddingLeft:15,paddingTop:5,paddingBottom:5,justifyContent:'space-between',alignItems:'center',borderTopWidth:1,borderTopColor:'#eee',backgroundColor:'white'},
    sendBtn:{backgroundColor:'$mainColor',marginTop:3},
    sendBtnText:{fontFamily:'$font',color:'white'},
    textArea:{width:'100%',textAlign:'right',fontFamily:'$font'},
    myChat:{maxWidth:'90%',backgroundColor:'$mainColor',width:'auto',paddingLeft:10,paddingRight:10,paddingTop:5,paddingBottom:5,borderRadius:10},
    otherChat:{maxWidth:'90%',backgroundColor:'#ddd',width:'auto',paddingLeft:10,paddingRight:10,paddingTop:5,paddingBottom:5,borderRadius:10},
    myChatText:{fontFamily:'$font',textAlign:'right'},
    empty:{fontFamily:'$font',textAlign:'right',marginTop:20,fontSize:13},
    otherChatText:{fontFamily:'$font',textAlign:'right'},
    myChatParent:{alignItems:'flex-end',marginTop:5},
    otherChatParent:{alignItems:'flex-start',marginTop:5},
})
export default styles = {
    login,signup,list,single,shopL,expert,createAd,filter,Fabrika,splashSC,chat
};